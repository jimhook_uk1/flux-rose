/**
 * ************************************************************************************************
 * Name: MainActivity
 * Workfile: MainActivity.java
 * Author: RDTek
 * Date: 07/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date   | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController Project.
 * 14/09/15 | RDTek | First Release.
 * 07/12/15 | RDTek | Added getIPAdd functionality to enable access to my computer running sim.
 *          |       | Added functionality to save current mode on phone orientation changes etc.
 * <p/>
 * Description:
 * ************************************************************************************************
 */
package rdteq.mooklight;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.support.v7.app.ActionBarActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import rdteq.mooklight.HelperClasses.DevNameAdapter;
import rdteq.mooklight.HelperClasses.Device;
import rdteq.mooklight.HelperClasses.WifiAP;
import rdteq.mooklight.HelperClasses.WifiScanner;
import rdteq.mooklight.MActivities.MControlLightActivity;

public class MainActivity extends ActionBarActivity {
    private static final String TAG = "Main____";

    private static final int CHANGE_NAME_ACTIVITY = 1;
    private static final int CONTROL_LIGHT_ACTIVITY = 2;
    private static final int TIMER_ENTRY_ACTIVITY = 3;

    private static final String COMPANY_MAC_ID = "00:80:a3";
    private static final String SSID_ID = "MookLight";

    private static ArrayList<Device> otherDevices;         /* Devices not on network or not associated yet */
    private static ArrayList<Device> devices;              /* List of devices on my network */
    private static Device connectedDevice;
    private static int mode;
    private static int position;
    //private static ProgressDialog progressDialog;

    private Activity activity;
    private Context context;
    private DevNameAdapter devNameAdapter;
    private int size;
    private Resources res;
    private WifiManager wifiManager;
    private WifiScanner wifiScanner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        activity = this;
        context = this;
        devices = new ArrayList<>();
        otherDevices = new ArrayList<>();
        size = 0;

        Display display = getWindowManager().getDefaultDisplay();
        final Point dispSize = new Point();
        display.getSize(dispSize);

        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiScannerEventsHandler wifiScannerEventsHandler = new WifiScannerEventsHandler();
        wifiScanner = new WifiScanner(activity, context, wifiScannerEventsHandler, wifiManager);

        // Asociate CustomAdapters with Listview
        ListView lv = (ListView) findViewById(R.id.list);
        res = getResources();
        AdapterEventsHandler adapterEventsHandler = new AdapterEventsHandler();
        devNameAdapter = new DevNameAdapter(this, devices, otherDevices, res, adapterEventsHandler);
        lv.setAdapter(devNameAdapter);

        readAssociatedDevices();
        if (devices.size() == 0) {
            mode = DevNameAdapter.ADD_DEVICE_MODE;
        } else {
            if (savedInstanceState != null) {
                mode = savedInstanceState.getInt("Saved_Mode");
            } else {
                mode = DevNameAdapter.CONTROL_LIGHT_MODE;
            }
        }
        devNameAdapter.setMode(mode);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (BuildConfig.DEBUG) Log.i(TAG, "onStart");

        /* Make sure wifi is enabled */
        wifiManager.setWifiEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) Log.i(TAG, "onResume");

        devNameAdapter.notifyDataSetChanged();
        wifiScanner.startScan();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) Log.i(TAG, "onPause");

        /* Stop scan */
        wifiScanner.stopScan();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) Log.i(TAG, "onSave");
        savedInstanceState.putInt("Saved_Mode", mode);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onStop() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onDestroy");

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.m_editDevName:
                mode = DevNameAdapter.EDIT_DEV_NAME_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();
                return true;
            case R.id.m_controlLight:
                mode = DevNameAdapter.CONTROL_LIGHT_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();
                return true;
            case R.id.m_addDevice:
                mode = DevNameAdapter.ADD_DEVICE_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();
                return true;
            case R.id.m_removeDevice:
                mode = DevNameAdapter.REMOVE_DEVICE_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();
                return true;
            case R.id.m_setTimer:
                mode = DevNameAdapter.SET_TIMER_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        wifiManager.disconnect();
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == wifiScanner.REQUEST_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Do something with granted permission
            wifiScanner.getScanningResults();
        }
    }

    private class AdapterEventsHandler implements DevNameAdapter.Events {
        @Override
        public void onConnectDeviceRequest(int pos) {
            Bundle bundle;
            Intent intent;

            position = pos;
            switch (mode) {
                case DevNameAdapter.EDIT_DEV_NAME_MODE:
                    connectedDevice = devices.get(pos);
                    intent = new Intent(MainActivity.this, ChangeNameActivity.class);
                    bundle = new Bundle();
                    bundle.putString("DevName", connectedDevice.devName);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CHANGE_NAME_ACTIVITY);
                    break;
                case DevNameAdapter.ADD_DEVICE_MODE:
                    connectedDevice = otherDevices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startControlLightActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    break;
                case DevNameAdapter.CONTROL_LIGHT_MODE:
                    connectedDevice = devices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startControlLightActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    break;
                case DevNameAdapter.SET_TIMER_MODE:
                    connectedDevice = devices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startTimerEntryActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    //wifiScanner.stopScan();
                    //progressDialog = new ProgressDialog(context);
                    //progressDialog.setMessage("Connecting..");
                    //progressDialog.setCancelable(false);
                    //progressDialog.show();
                    //connectedDevice = connectDevice;
                    //new WifiAPConnector(activity, context, wifiConnectorHdlr, wifiManager, connectedDevice);
                    break;
                case DevNameAdapter.REMOVE_DEVICE_MODE:
                    connectedDevice = devices.get(pos);
                    new RemoveDialog();
                    break;
            }
        }
    }

    private void startControlLightActivity() {
        Bundle bundle;
        Intent intent;

//        intent = new Intent(MainActivity.this, ControlLightActivity.class);

//        Changed the activity name, Mateen
        intent = new Intent(MainActivity.this, MControlLightActivity.class);
        bundle = new Bundle();
        bundle.putString("IPAddress", connectedDevice.ipAdd);
        bundle.putString("DevName", connectedDevice.devName);
        bundle.putInt("APType", connectedDevice.apType);
        bundle.putString("SSID", connectedDevice.SSID);
        bundle.putString("BSSID", connectedDevice.BSSID);
        intent.putExtras(bundle);
        startActivityForResult(intent, CONTROL_LIGHT_ACTIVITY);

    }

    private void startTimerEntryActivity() {
        Bundle bundle;
        Intent intent;

        intent = new Intent(MainActivity.this, TimerEntryActivity.class);
        bundle = new Bundle();
        bundle.putString("IPAddress", connectedDevice.ipAdd);
        bundle.putInt("APType", connectedDevice.apType);
        bundle.putString("SSID", connectedDevice.SSID);
        bundle.putString("BSSID", connectedDevice.BSSID);
        intent.putExtras(bundle);
        startActivityForResult(intent, TIMER_ENTRY_ACTIVITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHANGE_NAME_ACTIVITY:
                Bundle b = data.getExtras();
                connectedDevice.devName = b.getString("DevName");
                writeAssociatedDevices();
                break;
            case CONTROL_LIGHT_ACTIVITY:
                if ((mode == DevNameAdapter.ADD_DEVICE_MODE) &&
                        (resultCode == RESULT_OK)) {
                    devices.add(connectedDevice);
                    otherDevices.remove(position);
                    devNameAdapter.notifyDataSetChanged();
                    writeAssociatedDevices();
                    wifiManager.saveConfiguration();
                }
                break;
            case TIMER_ENTRY_ACTIVITY:
                break;
        }
    }

    private class WifiScannerEventsHandler implements WifiScanner.Events {
        @Override
        public void onScanComplete(ArrayList<WifiAP> apList) {
            boolean found;
            Device device;
            int i, j;
            String ssid;

            if (BuildConfig.DEBUG) Log.i(TAG, "Update Scan List");
            int newSize = apList.size();
            size = devices.size();
            if (size == 0) {
                otherDevices.clear();
                /* Compare new list with current list to see if there are any new APs */
                for (i = 0; i < newSize; i++) {
                    ssid = apList.get(i).SSID;
                    if (ssid.startsWith(SSID_ID)) {
                        device = new Device();
                        device.apType = apList.get(i).apType;
                        device.BSSID = apList.get(i).BSSID;
                        device.SSID = apList.get(i).SSID;
                        device.devName = device.SSID;
                        device.onLine = true;
                        device.ipAdd = getIPAdd(device.SSID);
                        otherDevices.add(device);
                        //}
                    }
                }
            } else {
                // Updating
                // Compare associated devices list with new list to see if any devices are missing
                for (i = 0; i < size; i++) {
                    found = false;
                    j = 0;
                    ssid = devices.get(i).SSID;
                    while ((j < newSize) && !found) {
                        if (apList.get(j).SSID.contentEquals(ssid)) {
                            devices.get(i).onLine = true;
                            found = true;
                        }
                        j++;
                    }
                    if (!found) {
                        devices.get(i).onLine = false;
                    }
                }
                /* Compare new list with current list to see if there are any new APs */
                otherDevices.clear();
                for (i = 0; i < newSize; i++) {
                    ssid = apList.get(i).SSID;
                    if (ssid.startsWith(SSID_ID)) {
                        found = false;
                        j = 0;
                        while ((j < size) && !found) {
                            if (devices.get(j).SSID.contentEquals(ssid)) {
                                found = true;
                            }
                            j++;
                        }
                        if (!found) {
                            device = new Device();
                            device.apType = apList.get(i).apType;
                            device.BSSID = apList.get(i).BSSID;
                            device.SSID = apList.get(i).SSID;
                            device.devName = device.SSID;
                            device.onLine = true;
                            device.ipAdd = getIPAdd(device.SSID);
                            otherDevices.add(device);
                        }
                    }
                }
            }
            devNameAdapter.notifyDataSetChanged();
        }
    }

    private class PasswordDialog {
        private PopupWindow popupWindow;

        private PasswordDialog() {
            Display display = activity.getWindowManager().getDefaultDisplay();
            final Point dispSize = new Point();
            display.getSize(dispSize);
            int dispWidth = dispSize.x;
            int centreY = dispSize.y / 2;
            LayoutInflater layoutInflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.password, (ViewGroup) findViewById(R.id.password_element));
            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);

            final EditText input = (EditText) popupView.findViewById(R.id.password);
            input.setTransformationMethod(PasswordTransformationMethod.getInstance());
            CheckBox showPass = (CheckBox) popupView.findViewById(R.id.showPassword);
            Button okButton = (Button) popupView.findViewById(R.id.passwordDone);
            Button cancelButton = (Button) popupView.findViewById(R.id.passwordCancel);

            showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        // show password
                        input.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        input.setSelection(input.getText().length());
                    } else {
                        // hide password
                        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        input.setSelection(input.getText().length());
                    }
                }
            });

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    if (connectedDevice.apType == WifiAP.WEP_AP) {
                        WifiConfiguration wifiConfiguration = new WifiConfiguration();
                        wifiConfiguration.SSID = "\"" + connectedDevice.SSID + "\"";
                        wifiConfiguration.wepKeys[0] = "\"" + input.getText().toString() + "\"";
                        wifiConfiguration.wepTxKeyIndex = 0;
                        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                        wifiManager.addNetwork(wifiConfiguration);
                    } else {
                        WifiConfiguration wifiConfiguration = new WifiConfiguration();
                        wifiConfiguration.SSID = "\"" + connectedDevice.SSID + "\"";
                        wifiConfiguration.preSharedKey = "\"" + input.getText().toString() + "\"";
                        wifiManager.addNetwork(wifiConfiguration);
                    }
                    switch (mode) {
                        case DevNameAdapter.ADD_DEVICE_MODE:
                            startControlLightActivity();
                            break;
                        case DevNameAdapter.CONTROL_LIGHT_MODE:
                            startControlLightActivity();
                            break;
                        case DevNameAdapter.SET_TIMER_MODE:
                            startTimerEntryActivity();
                            break;
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

            int y = centreY / 2;
            popupWindow.setWidth(dispWidth - 20);
            popupWindow.showAsDropDown(popupView, 10, y);
        }
    }

    private class RemoveDialog {
        private PopupWindow popupWindow;

        private RemoveDialog() {
            Display display = activity.getWindowManager().getDefaultDisplay();
            final Point dispSize = new Point();
            display.getSize(dispSize);
            int dispWidth = dispSize.x;
            int centreY = dispSize.y / 2;
            LayoutInflater layoutInflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.remove_device, (ViewGroup)findViewById(R.id.remove_element));
            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);

            TextView name = (TextView) popupView.findViewById(R.id.removeDevice);
            name.setText(connectedDevice.devName);
            Button yesButton = (Button) popupView.findViewById(R.id.removeOK);
            Button noButton = (Button) popupView.findViewById(R.id.removeCancel);

            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    devices.remove(position);
                    devNameAdapter.notifyDataSetChanged();
                    writeAssociatedDevices();
                }
            });

            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

            int y = centreY / 2;
            popupWindow.setWidth(dispWidth - 20);
            popupWindow.showAsDropDown(popupView, 10, y);
        }
    }

    private void getPassWordAndConnect(Device device) {
        if (device.apType == WifiAP.WPA_AP) {
            new PasswordDialog();
        } else if (device.apType == WifiAP.WEP_AP) {
            new PasswordDialog();
        } else {
            WifiConfiguration wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = "\"" + device.SSID + "\"";
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiManager.addNetwork(wifiConfiguration);
        }
    }

    private boolean isDeviceRegistered(Device device) {
        boolean found;
        int i;
        List<WifiConfiguration> attachedAps;
        String ssidWithQuotes;

        // Check if we're trying to connect to a registered network
        attachedAps = wifiManager.getConfiguredNetworks();
        i = 0;
        found = false;
        ssidWithQuotes = "\"" + device.SSID + "\"";
        if (BuildConfig.DEBUG) Log.i(TAG, "Connecting to: " + device.SSID);
        while ((i < attachedAps.size()) &&
                !found) {
            if (ssidWithQuotes.contentEquals(attachedAps.get(i).SSID)) {
                found = true;
            } else {
                i++;
            }
        }
        return found;
    }

    /*
     * Reads saved device data from the file "Devices"
     * Data is stored in the following order for each device:
     * SSID, BSSID, devName, apType
     */
    private void readAssociatedDevices() {
        /* Load Associated List from File */
        try {
            int i;
            String inLine;
            Device device;

            File file = new File(context.getFilesDir(), "Devices");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            i = 0;
            device = new Device();
            inLine = reader.readLine();
            while ((inLine != null) && !inLine.contains("Empty")){
                switch (i) {
                    case 0: /* SSID */
                        device.SSID = inLine;
                        i++;
                        break;
                    case 1: /* BSSID */
                        device.BSSID = inLine;
                        i++;
                        break;
                    case 2: /* devName */
                        device.devName = inLine;
                        i++;
                        break;
                    case 3: /* apType */
                        device.apType = Integer.valueOf(inLine);
                        device.ipAdd = getIPAdd(device.SSID);
                        devices.add(device);
                        device = new Device();
                        i = 0;
                        break;
                }
                inLine = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException fnfe) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Device File Not Found");
        } catch (IOException ioe) {
            if (BuildConfig.DEBUG) Log.e(TAG, "Error Reading File", ioe);
        }
    }

    /*
     * Writes device data to the file "Devices"
     * Data is stored in the following order for each device:
     * SSID, BSSID, devName, apType
     */
    private void writeAssociatedDevices() {
        if (BuildConfig.DEBUG) Log.i(TAG, "Storing Devices");
        try {
            File file = new File(context.getFilesDir(), "Devices");
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
            Device device;
            int i;

            if (devices.size() != 0) {
                for (i = 0; i < devices.size(); i++) {
                    device = devices.get(i);
                    writer.write(device.SSID);
                    writer.newLine();
                    writer.write(device.BSSID);
                    writer.newLine();
                    writer.write(device.devName);
                    writer.newLine();
                    writer.write(String.valueOf(device.apType));
                    writer.newLine();
                }
                if (BuildConfig.DEBUG) Log.i(TAG, "Records Written: " + String.valueOf(i));
            } else {
                writer.write("Empty");
                writer.newLine();
                if (BuildConfig.DEBUG) Log.i(TAG, "Records Written: 0");
            }
            writer.close();
        } catch (IOException ioe) {
            if (BuildConfig.DEBUG) Log.e(TAG, "Couldn't open file: ", ioe);
        }
    }

    private String getIPAdd(String ssid) {
        String rv;

        if (ssid.equals("MookLight_1123456")) {
            rv = "192.168.1.64";
        } else {
            rv = res.getString(R.string.s_serverIP);
        }
        return rv;
    }
}
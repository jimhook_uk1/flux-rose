package rdteq.mooklight.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Timer;

import rdteq.mooklight.HelperClasses.Device;
import rdteq.mooklight.HelperClasses.DeviceComms;
import rdteq.mooklight.R;

public class ControlLightFragment extends Fragment {

    private static final String TAG = "CtrlLamp";

    static final int JUST_STARTED = 0;
    static final int WAITING_DIM = 1;
    static final int GETTING_ONOFF = 2;
    static final int WAITING_ONOFF = 3;
    static final int RUNNING = 4;
    static final int SHUT_DOWN = 5;

    static final int BRIGHTNESS_OFFSET = 10;

    static final int TIMER_TASK_DELAY = 100; //msecs
    static final int TIMER_TASK_PERIOD = 200; //msecs
    DeviceComms devComms;
    Resources res;

    private int brightness;
    private int commsState;
    private int onoff;
    private Context context;
    private Device connectedDevice;
    private Button onoffButton;
    private FrameLayout controlFLayout;
    private ProgressDialog progressDialog;
    private SeekBar brightnessSeekBar;
    private TextView brightnessStatus;
    private TextView onoffStatus;
    private Timer commsTimer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_control_light, container, false);

        context = getContext();
        connectedDevice = new Device();
        res = getResources();
        return rootView;
    }
}

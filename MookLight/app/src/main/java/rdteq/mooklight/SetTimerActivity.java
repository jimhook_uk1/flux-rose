package rdteq.mooklight;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;


import java.util.ArrayList;

import rdteq.mooklight.Adapters.CustomAdapter;

public class SetTimerActivity extends AppCompatActivity {

    CustomAdapter customAdapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_timer);

        listView  = (ListView) findViewById(R.id.list) ;

        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i < 2; i++){
            list.add("10:21");
        }

        customAdapter = new CustomAdapter(list, this);
        listView.setAdapter(customAdapter);
    }
}

package rdteq.mooklight.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

import rdteq.mooklight.R;

/**
 * Created by Umair Saeed on 7/17/2017.
 */

public class CustomAdapter extends ArrayAdapter<String> {
    private ArrayList<String> dataSet;
    Context mContext;

    public CustomAdapter(ArrayList<String> data, Context context) {
        super(context, R.layout.timer_items, data);
        this.dataSet = data;
        this.mContext=context;

    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.timer_items, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.time);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }



        viewHolder.txtName.setText(dataSet.get(position));
        // Return the completed view to render on screen
        return convertView;
    }
}

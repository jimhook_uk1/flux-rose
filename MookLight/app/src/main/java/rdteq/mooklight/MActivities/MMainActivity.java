package rdteq.mooklight.MActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rdteq.mooklight.BuildConfig;
import rdteq.mooklight.ChangeNameActivity;
import rdteq.mooklight.HelperClasses.DevNameAdapter;
import rdteq.mooklight.HelperClasses.Device;
import rdteq.mooklight.HelperClasses.WifiAP;
import rdteq.mooklight.HelperClasses.WifiScanner;
import rdteq.mooklight.MainActivity;
import rdteq.mooklight.R;
import rdteq.mooklight.TimerEntryActivity;

public class MMainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Mateen_Main____";

    private static final int CHANGE_NAME_ACTIVITY = 1;
    private static final int CONTROL_LIGHT_ACTIVITY = 2;
    private static final int TIMER_ENTRY_ACTIVITY = 3;

    private static final String COMPANY_MAC_ID = "00:80:a3";
    private static final String SSID_ID = "MookLight";

    private static ArrayList<Device> otherDevices;         /* Devices not on network or not associated yet */
    private static ArrayList<Device> devices;              /* List of devices on my network */
    private static Device connectedDevice;
    private static int mode;
    private static int position;
    //private static ProgressDialog progressDialog;

    private Activity activity;
    private Context context;
    private DevNameAdapter devNameAdapter;
    private int size;
    private Resources res;
    private WifiManager wifiManager;
    private WifiScanner wifiScanner;

    ImageView bulb;
    ImageView knob_;
    ImageView plus;
    ImageView clock;
    ImageView trash_bin;

    ImageView one;
    ImageView two;
    ImageView three;
    ImageView four;
    ImageView five;
    String resultStr = "";
    RelativeLayout rel_popup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_mmain);

        rel_popup = (RelativeLayout) findViewById(R.id.activity_mmain);

        activity = this;
        context = this;
        devices = new ArrayList<>();
        otherDevices = new ArrayList<>();
        size = 0;

        bulb = (ImageView) findViewById(R.id.bulb);
        knob_ = (ImageView) findViewById(R.id.knob_);
        plus = (ImageView) findViewById(R.id.plus);
        clock = (ImageView) findViewById(R.id.clock);
        trash_bin = (ImageView) findViewById(R.id.trash_bin);

        one = (ImageView) findViewById(R.id.one);
        two = (ImageView) findViewById(R.id.two);
        three = (ImageView) findViewById(R.id.three);
        four = (ImageView) findViewById(R.id.four);
        five = (ImageView) findViewById(R.id.five);

        bulb.setOnClickListener(this);
        knob_.setOnClickListener(this);
        plus.setOnClickListener(this);
        clock.setOnClickListener(this);
        trash_bin.setOnClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        final Point dispSize = new Point();
        display.getSize(dispSize);

        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        MMainActivity.WifiScannerEventsHandler wifiScannerEventsHandler = new MMainActivity.WifiScannerEventsHandler();
        wifiScanner = new WifiScanner(activity, context, wifiScannerEventsHandler, wifiManager);

        // Asociate CustomAdapters with Listview
        ListView lv = (ListView) findViewById(R.id.list);
        res = getResources();
        MMainActivity.AdapterEventsHandler adapterEventsHandler = new MMainActivity.AdapterEventsHandler();
        devNameAdapter = new DevNameAdapter(this, devices, otherDevices, res, adapterEventsHandler);
        lv.setAdapter(devNameAdapter);

        readAssociatedDevices();
        if (devices.size() == 0) {
            mode = DevNameAdapter.ADD_DEVICE_MODE;
        } else {
            if (savedInstanceState != null) {
                mode = savedInstanceState.getInt("Saved_Mode");
            } else {
                mode = DevNameAdapter.CONTROL_LIGHT_MODE;
            }
        }
        devNameAdapter.setMode(mode);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (BuildConfig.DEBUG) Log.i(TAG, "onStart");

        /* Make sure wifi is enabled */
        wifiManager.setWifiEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) Log.i(TAG, "onResume");

        devNameAdapter.notifyDataSetChanged();
        wifiScanner.startScan();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) Log.i(TAG, "onPause");

        /* Stop scan */
        wifiScanner.stopScan();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) Log.i(TAG, "onSave");
        savedInstanceState.putInt("Saved_Mode", mode);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onStop() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onDestroy");

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        wifiManager.disconnect();
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bulb:
                mode = DevNameAdapter.EDIT_DEV_NAME_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();

                one.setVisibility(View.VISIBLE);
                two.setVisibility(View.INVISIBLE);
                three.setVisibility(View.INVISIBLE);
                four.setVisibility(View.INVISIBLE);
                five.setVisibility(View.INVISIBLE);

                bulb.setImageResource(R.drawable.bulb_unselected);
                knob_.setImageResource(R.drawable.knob_unselected);
                plus.setImageResource(R.drawable.plus_unselected);
                clock.setImageResource(R.drawable.clock_unselected);
                trash_bin.setImageResource(R.drawable.trash_unselected);

                break;
            case R.id.knob_:
                mode = DevNameAdapter.CONTROL_LIGHT_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();

                one.setVisibility(View.INVISIBLE);
                two.setVisibility(View.VISIBLE);
                three.setVisibility(View.INVISIBLE);
                four.setVisibility(View.INVISIBLE);
                five.setVisibility(View.INVISIBLE);

                bulb.setImageResource(R.drawable.bulb_unselected);
                knob_.setImageResource(R.drawable.knob_selected);
                plus.setImageResource(R.drawable.plus_unselected);
                clock.setImageResource(R.drawable.clock_unselected);
                trash_bin.setImageResource(R.drawable.trash_unselected);

                break;
            case R.id.plus:
                mode = DevNameAdapter.ADD_DEVICE_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();

                one.setVisibility(View.INVISIBLE);
                two.setVisibility(View.INVISIBLE);
                three.setVisibility(View.VISIBLE);
                four.setVisibility(View.INVISIBLE);
                five.setVisibility(View.INVISIBLE);

                bulb.setImageResource(R.drawable.bulb_unselected);
                knob_.setImageResource(R.drawable.knob_unselected);
                plus.setImageResource(R.drawable.plus_selected);
                clock.setImageResource(R.drawable.clock_unselected);
                trash_bin.setImageResource(R.drawable.trash_unselected);

                break;
            case R.id.clock:
                mode = DevNameAdapter.SET_TIMER_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();

                one.setVisibility(View.INVISIBLE);
                two.setVisibility(View.INVISIBLE);
                three.setVisibility(View.INVISIBLE);
                four.setVisibility(View.VISIBLE);
                five.setVisibility(View.INVISIBLE);

                bulb.setImageResource(R.drawable.bulb_unselected);
                knob_.setImageResource(R.drawable.knob_unselected);
                plus.setImageResource(R.drawable.plus_unselected);
                clock.setImageResource(R.drawable.clock_selected);
                trash_bin.setImageResource(R.drawable.trash_unselected);

                break;
            case R.id.trash_bin:
                mode = DevNameAdapter.REMOVE_DEVICE_MODE;
                devNameAdapter.setMode(mode);
                devNameAdapter.notifyDataSetChanged();


                one.setVisibility(View.INVISIBLE);
                two.setVisibility(View.INVISIBLE);
                three.setVisibility(View.INVISIBLE);
                four.setVisibility(View.INVISIBLE);
                five.setVisibility(View.VISIBLE);

                bulb.setImageResource(R.drawable.bulb_unselected);
                knob_.setImageResource(R.drawable.knob_unselected);
                plus.setImageResource(R.drawable.plus_unselected);
                clock.setImageResource(R.drawable.clock_unselected);
                trash_bin.setImageResource(R.drawable.trash_selected);

                break;
            default:
                Log.d("Mateen", "Default Called....");
        }
    }

    private class AdapterEventsHandler implements DevNameAdapter.Events {
        @Override
        public void onConnectDeviceRequest(int pos) {
            Bundle bundle;
            Intent intent;

            position = pos;
            switch (mode) {
                case DevNameAdapter.EDIT_DEV_NAME_MODE:
                    connectedDevice = devices.get(pos);
                    intent = new Intent(MMainActivity.this, MChangeNameActivity.class);
                    bundle = new Bundle();
                    bundle.putString("DevName", connectedDevice.devName);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, CHANGE_NAME_ACTIVITY);
                    break;
                case DevNameAdapter.ADD_DEVICE_MODE:
                    connectedDevice = otherDevices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startControlLightActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    break;
                case DevNameAdapter.CONTROL_LIGHT_MODE:
                    connectedDevice = devices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startControlLightActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    break;
                case DevNameAdapter.SET_TIMER_MODE:
                    connectedDevice = devices.get(pos);
                    if (isDeviceRegistered(connectedDevice)) {
                        startTimerEntryActivity();
                    } else {
                        getPassWordAndConnect(connectedDevice);
                    }
                    //wifiScanner.stopScan();
                    //progressDialog = new ProgressDialog(context);
                    //progressDialog.setMessage("Connecting..");
                    //progressDialog.setCancelable(false);
                    //progressDialog.show();
                    //connectedDevice = connectDevice;
                    //new WifiAPConnector(activity, context, wifiConnectorHdlr, wifiManager, connectedDevice);
                    break;
                case DevNameAdapter.REMOVE_DEVICE_MODE:
                    connectedDevice = devices.get(pos);
                    new MMainActivity.RemoveDialog();
                    break;
            }
        }
    }

    private void startControlLightActivity() {
        Bundle bundle;
        Intent intent;

//        intent = new Intent(MainActivity.this, ControlLightActivity.class);

//        Changed the activity name, Mateen
        intent = new Intent(MMainActivity.this, MControlLightActivity.class);
        bundle = new Bundle();
        bundle.putString("IPAddress", connectedDevice.ipAdd);
        bundle.putString("DevName", connectedDevice.devName);
        bundle.putInt("APType", connectedDevice.apType);
        bundle.putString("SSID", connectedDevice.SSID);
        bundle.putString("BSSID", connectedDevice.BSSID);
        intent.putExtras(bundle);
        startActivityForResult(intent, CONTROL_LIGHT_ACTIVITY);

    }

    private void startTimerEntryActivity() {
        Bundle bundle;
        Intent intent;

        intent = new Intent(MMainActivity.this, MTimerEntryActivity.class);
        bundle = new Bundle();
        bundle.putString("IPAddress", connectedDevice.ipAdd);
        bundle.putInt("APType", connectedDevice.apType);
        bundle.putString("SSID", connectedDevice.SSID);
        bundle.putString("BSSID", connectedDevice.BSSID);
        intent.putExtras(bundle);
        startActivityForResult(intent, TIMER_ENTRY_ACTIVITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CHANGE_NAME_ACTIVITY:
                Bundle b = data.getExtras();
                connectedDevice.devName = b.getString("DevName");
                writeAssociatedDevices();
                break;
            case CONTROL_LIGHT_ACTIVITY:
                if ((mode == DevNameAdapter.ADD_DEVICE_MODE) &&
                        (resultCode == RESULT_OK)) {
                    devices.add(connectedDevice);
                    otherDevices.remove(position);
                    devNameAdapter.notifyDataSetChanged();
                    writeAssociatedDevices();
                    wifiManager.saveConfiguration();
                }
                break;
            case TIMER_ENTRY_ACTIVITY:
                break;
        }
    }

    private class WifiScannerEventsHandler implements WifiScanner.Events {
        @Override
        public void onScanComplete(ArrayList<WifiAP> apList) {
            boolean found;
            Device device;
            int i, j;
            String ssid;

            if (BuildConfig.DEBUG) Log.i(TAG, "Update Scan List");
            int newSize = apList.size();
            size = devices.size();
            if (size == 0) {
                otherDevices.clear();
                /* Compare new list with current list to see if there are any new APs */
                for (i = 0; i < newSize; i++) {
                    ssid = apList.get(i).SSID;
                    if (ssid.startsWith(SSID_ID)) {
                        device = new Device();
                        device.apType = apList.get(i).apType;
                        device.BSSID = apList.get(i).BSSID;
                        device.SSID = apList.get(i).SSID;
                        device.devName = device.SSID;
                        device.onLine = true;
                        device.ipAdd = getIPAdd(device.SSID);
                        otherDevices.add(device);
                        //}
                    }
                }
            } else {
                // Updating
                // Compare associated devices list with new list to see if any devices are missing
                for (i = 0; i < size; i++) {
                    found = false;
                    j = 0;
                    ssid = devices.get(i).SSID;
                    while ((j < newSize) && !found) {
                        if (apList.get(j).SSID.contentEquals(ssid)) {
                            devices.get(i).onLine = true;
                            found = true;
                        }
                        j++;
                    }
                    if (!found) {
                        devices.get(i).onLine = false;
                    }
                }
                /* Compare new list with current list to see if there are any new APs */
                otherDevices.clear();
                for (i = 0; i < newSize; i++) {
                    ssid = apList.get(i).SSID;
                    if (ssid.startsWith(SSID_ID)) {
                        found = false;
                        j = 0;
                        while ((j < size) && !found) {
                            if (devices.get(j).SSID.contentEquals(ssid)) {
                                found = true;
                            }
                            j++;
                        }
                        if (!found) {
                            device = new Device();
                            device.apType = apList.get(i).apType;
                            device.BSSID = apList.get(i).BSSID;
                            device.SSID = apList.get(i).SSID;
                            device.devName = device.SSID;
                            device.onLine = true;
                            device.ipAdd = getIPAdd(device.SSID);
                            otherDevices.add(device);
                        }
                    }
                }
            }
            devNameAdapter.notifyDataSetChanged();
        }
    }

    private class PasswordDialog {
        private PopupWindow popupWindow;

        private PasswordDialog(RelativeLayout rel_popup) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.password_layout_m, (ViewGroup) findViewById(R.id.password_element_mm));
            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);

            final EditText pwText = (EditText) popupView.findViewById(R.id.pw_text);

//            final EditText input = (EditText) popupView.findViewById(R.id.password);
//            input.setTransformationMethod(PasswordTransformationMethod.getInstance());
//            CheckBox showPass = (CheckBox) popupView.findViewById(R.id.showPassword);
            ImageView okButton = (ImageView) popupView.findViewById(R.id.pair_devm);
            ImageView cancelButton = (ImageView) popupView.findViewById(R.id.back);

//            showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked) {
//                        // show password
//                        input.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
//                        input.setSelection(input.getText().length());
//                    } else {
//                        // hide password
//                        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
//                        input.setSelection(input.getText().length());
//                    }
//                }
//            });

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    //if (resultStr.length() > 0) {
//                        resultStr = "12345678";
                        if (connectedDevice.apType == WifiAP.WEP_AP) {
                            WifiConfiguration wifiConfiguration = new WifiConfiguration();
                            wifiConfiguration.SSID = "\"" + connectedDevice.SSID + "\"";
                            wifiConfiguration.wepKeys[0] = "\"" + pwText.getText().toString() + "\"";
                            wifiConfiguration.wepTxKeyIndex = 0;
                            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                            wifiManager.addNetwork(wifiConfiguration);
                        } else {
                            WifiConfiguration wifiConfiguration = new WifiConfiguration();
                            wifiConfiguration.SSID = "\"" + connectedDevice.SSID + "\"";
                            wifiConfiguration.preSharedKey = "\"" + pwText.getText().toString() + "\"";
                            wifiManager.addNetwork(wifiConfiguration);
                        }
                        switch (mode) {
                            case DevNameAdapter.ADD_DEVICE_MODE:
                                startControlLightActivity();
                                break;
                            case DevNameAdapter.CONTROL_LIGHT_MODE:
                                startControlLightActivity();
                                break;
                            case DevNameAdapter.SET_TIMER_MODE:
                                startTimerEntryActivity();
                                break;
                        }
                    //}
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resultStr = "";
                    popupWindow.dismiss();
                }
            });

//            int y = centreY / 2;
//            popupWindow.setWidth(dispWidth - 20);
//            popupWindow.showAsDropDown(popupView, 10, y);

            popupWindow.showAtLocation(rel_popup, Gravity.NO_GRAVITY, 0, 0);
        }
    }

    private class RemoveDialog {
        private PopupWindow popupWindow;

        private RemoveDialog() {
            Display display = activity.getWindowManager().getDefaultDisplay();
            final Point dispSize = new Point();
            display.getSize(dispSize);
            int dispWidth = dispSize.x;
            int centreY = dispSize.y / 2;
            LayoutInflater layoutInflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //View popupView = layoutInflater.inflate(R.layout.remove_device_m, (ViewGroup)findViewById(R.id.remove_element_m));
            View popupView = layoutInflater.inflate(R.layout.remove_device_m, null);
            popupWindow = new PopupWindow(
                    popupView,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(false);

            TextView name = (TextView) popupView.findViewById(R.id.removeDevice);
            name.setText(connectedDevice.devName);
            Button yesButton = (Button) popupView.findViewById(R.id.removeOK);
            Button noButton = (Button) popupView.findViewById(R.id.removeCancel);

            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    devices.remove(position);
                    devNameAdapter.notifyDataSetChanged();
                    writeAssociatedDevices();
                }
            });

            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

            int y = centreY / 2;
            popupWindow.setWidth(dispWidth - 20);
            popupWindow.showAsDropDown(popupView, 10, y);
        }
    }

    private void getPassWordAndConnect(Device device) {
        if (device.apType == WifiAP.WPA_AP) {
            new MMainActivity.PasswordDialog(rel_popup);
        } else if (device.apType == WifiAP.WEP_AP) {
            new MMainActivity.PasswordDialog(rel_popup);
        } else {
            WifiConfiguration wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = "\"" + device.SSID + "\"";
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiManager.addNetwork(wifiConfiguration);
        }
    }

    private boolean isDeviceRegistered(Device device) {
        boolean found;
        int i;
        List<WifiConfiguration> attachedAps;
        String ssidWithQuotes;

        // Check if we're trying to connect to a registered network
        attachedAps = wifiManager.getConfiguredNetworks();
        i = 0;
        found = false;
        ssidWithQuotes = "\"" + device.SSID + "\"";
        if (BuildConfig.DEBUG) Log.i(TAG, "Connecting to: " + device.SSID);
        while ((i < attachedAps.size()) &&
                !found) {
            if (ssidWithQuotes.contentEquals(attachedAps.get(i).SSID)) {
                found = true;
            } else {
                i++;
            }
        }
        return found;
    }

    /*
     * Reads saved device data from the file "Devices"
     * Data is stored in the following order for each device:
     * SSID, BSSID, devName, apType
     */
    private void readAssociatedDevices() {
        /* Load Associated List from File */
        try {
            int i;
            String inLine;
            Device device;

            File file = new File(context.getFilesDir(), "Devices");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            i = 0;
            device = new Device();
            inLine = reader.readLine();
            while ((inLine != null) && !inLine.contains("Empty")){
                switch (i) {
                    case 0: /* SSID */
                        device.SSID = inLine;
                        i++;
                        break;
                    case 1: /* BSSID */
                        device.BSSID = inLine;
                        i++;
                        break;
                    case 2: /* devName */
                        device.devName = inLine;
                        i++;
                        break;
                    case 3: /* apType */
                        device.apType = Integer.valueOf(inLine);
                        device.ipAdd = getIPAdd(device.SSID);
                        devices.add(device);
                        device = new Device();
                        i = 0;
                        break;
                }
                inLine = reader.readLine();
            }
            reader.close();
        } catch (FileNotFoundException fnfe) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Device File Not Found");
        } catch (IOException ioe) {
            if (BuildConfig.DEBUG) Log.e(TAG, "Error Reading File", ioe);
        }
    }

    /*
     * Writes device data to the file "Devices"
     * Data is stored in the following order for each device:
     * SSID, BSSID, devName, apType
     */
    private void writeAssociatedDevices() {
        if (BuildConfig.DEBUG) Log.i(TAG, "Storing Devices");
        try {
            File file = new File(context.getFilesDir(), "Devices");
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
            Device device;
            int i;

            if (devices.size() != 0) {
                for (i = 0; i < devices.size(); i++) {
                    device = devices.get(i);
                    writer.write(device.SSID);
                    writer.newLine();
                    writer.write(device.BSSID);
                    writer.newLine();
                    writer.write(device.devName);
                    writer.newLine();
                    writer.write(String.valueOf(device.apType));
                    writer.newLine();
                }
                if (BuildConfig.DEBUG) Log.i(TAG, "Records Written: " + String.valueOf(i));
            } else {
                writer.write("Empty");
                writer.newLine();
                if (BuildConfig.DEBUG) Log.i(TAG, "Records Written: 0");
            }
            writer.close();
        } catch (IOException ioe) {
            if (BuildConfig.DEBUG) Log.e(TAG, "Couldn't open file: ", ioe);
        }
    }

    private String getIPAdd(String ssid) {
        String rv;

        if (ssid.equals("MookLight_1123456")) {
            rv = "192.168.1.64";
        } else {
            rv = res.getString(R.string.s_serverIP);
        }
        return rv;
    }
}

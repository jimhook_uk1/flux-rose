/**
 * *************************************************************************************************
 * Name: Timer Entry
 * Workfile: TimerEntryActivity.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Modified for persistent communications
 * 12/12/15 | RDTek | Made sure comms are closed after ReadTimers2.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.MActivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import rdteq.mooklight.BuildConfig;
import rdteq.mooklight.ChangeTimerActivity;
import rdteq.mooklight.HelperClasses.Constants;
import rdteq.mooklight.HelperClasses.Device;
import rdteq.mooklight.HelperClasses.DeviceComms;
import rdteq.mooklight.R;

import static rdteq.mooklight.R.drawable.back;
import static rdteq.mooklight.R.drawable.bar_black2;
import static rdteq.mooklight.R.drawable.bar_blue2;

public class MTimerEntryActivity extends ActionBarActivity {
    private static final String TAG = "TimerEnt";

    private static final int TIMER_CHANGE_ACTIVITY = 1;

    private static final int NUM_ADAPTER_ROWS = 14;
    private static final int NUM_TIMERS = 30;

    private Activity activity;
    private boolean finishing;
    private boolean timersRead;
    private Context context;
    private DeviceComms devComms;
    private int timerIdx;
    private int timers[] = new int[NUM_TIMERS];
    private ProgressDialog progressDialog;
    private Resources res;
    private TimerEntryAdapter timerEntryAdapter;

    private static LayoutInflater inflater = null;

    ImageView back_btn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_set_timer);
        activity = this;
        context = this;
        Device connectedDevice = new Device();
        ListView lv = (ListView) findViewById(R.id.timerList);
        res = getResources();

        back_btn = (ImageView) findViewById(R.id.back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MTimerEntryActivity.this, MMainActivity.class);
                startActivity(intent);
                finish();
            }
        });



        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        connectedDevice.ipAdd = b.getString("IPAddress");
        connectedDevice.apType = b.getInt("APType");
        connectedDevice.SSID = b.getString("SSID");
        connectedDevice.BSSID = b.getString("BSSID");

        // Initialise Timers
        for (int i = 2; i < NUM_TIMERS; i += 2) {
            timers[i] = 0xFFFF;
        }
        if (savedInstanceState != null) {
            timersRead = savedInstanceState.getBoolean("Saved_TimersRead");
            if (timersRead) {
                timers = savedInstanceState.getIntArray("Saved_Timers");
            }
        }

        timerEntryAdapter = new TimerEntryAdapter();
        lv.setAdapter(timerEntryAdapter);
        View vi = inflater.inflate(R.layout.row_done_button, (ViewGroup)findViewById(R.id.timerList), false);
        lv.addFooterView(vi);
        Button doneButton = (Button) vi.findViewById(R.id.buttonDone);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishWithResult();
            }
        });

        devComms = new DeviceComms(context, devCommsMsgHdlr, connectedDevice);
    }

    @Override
    public void onResume() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onResume");
        super.onResume();
        if (timersRead) {
            timerEntryAdapter.notifyDataSetChanged();
        } else {
            devComms.connect();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Connecting..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    public void onPause() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onPause");
        super.onPause();
        if (finishing) {
            devComms.close();
        }
    }

    @Override
    public void onStop() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) Log.i(TAG, "onSave");
        savedInstanceState.putBoolean("Saved_TimersRead", timersRead);
        savedInstanceState.putIntArray("Saved_Timers", timers);

        super.onSaveInstanceState(savedInstanceState);
    }

    private void finishWithResult() {
        if (BuildConfig.DEBUG) Log.i(TAG, "Finish");
        /* Set current time */
        Time time = new Time();
        time.setToNow();
        timers[0] = time.weekDay;
        timers[1] = time.hour * 60 + time.minute;
        finishing = true;
        devComms.connect();
    }

    private Handler devCommsMsgHdlr = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int i, j;
            int rxdData[];

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            switch (msg.what) {
                case DeviceComms.ABORTED:
                    exitActivity(RESULT_CANCELED);
                    break;

                case DeviceComms.CONNECTED:
                    if (!finishing) {
                        devComms.readTimers1();
                    } else {
                        int txData[] = new int[16];
                        for (i = 0; i < 16; i++) {
                            txData[i] = timers[i];
                        }
                        devComms.writeTimers1(txData);
                    }
                    break;

                case DeviceComms.XFER_ERROR:
                    String error;
                    switch (msg.arg1) {
                        case Constants.LOST_CONNECTION:
                            error = res.getString(R.string.s_lostConnection);
                            break;
                        case Constants.READ_IO_EXCEPTION:
                            error = res.getString(R.string.s_readIOException);
                            break;
                        case Constants.SOCKET_INIT_IO_EXCEPTION:
                            error = res.getString(R.string.s_socketInitIOException);
                            break;
                        case Constants.STREAM_INIT_IO_EXCEPTION:
                            error = res.getString(R.string.s_streamInitIOException);
                            break;
                        case Constants.UNKNOWN_HOST_EXCEPTION:
                            error = res.getString(R.string.s_unknownHostException);
                            break;
                        case Constants.WRITE_IO_EXCEPTION:
                            error = res.getString(R.string.s_writeIOException);
                            break;
                        default:
                            error = res.getString(R.string.s_unknownError);
                            break;
                    }
                    Toast.makeText(context.getApplicationContext(), error, Toast.LENGTH_LONG).show();
                    exitActivity(RESULT_CANCELED);
                    break;

                case DeviceComms.TID_READ_TIMERS1:
                    rxdData = (int[]) msg.obj;
                    for (i = 0; i < rxdData.length; i++) {
                        timers[i] = rxdData[i];
                    }
                    devComms.readTimers2();
                    break;

                case DeviceComms.TID_READ_TIMERS2:
                    rxdData = (int[]) msg.obj;
                    j = 16;
                    for (i = 0; i < 14; i++) {
                        timers[j] = rxdData[i];
                        j++;
                    }
                    timersRead = true;
                    timerEntryAdapter.notifyDataSetChanged();
                    devComms.close();
                    break;

                case DeviceComms.TID_WRITE_TIMERS1:
                    int txData[] = new int[14];
                    j = 16;
                    for (i = 0; i < 14; i++) {
                        txData[i] = timers[j];
                        j++;
                    }
                    devComms.writeTimers2(txData);
                    break;

                case DeviceComms.TID_WRITE_TIMERS2:
                    devComms.close();
                    exitActivity(RESULT_OK);
                    break;

            }
        }
    };

    private void exitActivity(int result) {
        Bundle b = new Bundle();
        b.putIntArray("TimersArray", timers);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(result, intent);
        finish();
    }

    private class TimerEntryAdapter extends BaseAdapter {

        /**
         * **********  DevNameAdapter Constructor ****************
         */
        public TimerEntryAdapter() {
            inflater = (LayoutInflater) activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        /**
         * ***** What is the size of Passed Arraylist Size ***********
         */
        public int getCount() {
            return NUM_ADAPTER_ROWS;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /**
         * ****** Create a holder Class to contain inflated xml file elements ********
         */
        public class ViewHolder {
//            public LinearLayout timerRow;
//            public TextView timerName;

            public TextView time_tv;
            public TextView days_name;
            public TextView am_pm;
            public ImageView ability;
        }

        /**
         * *** Depends upon data size called for each row , Create each ListView row ****
         */
        public View getView(int position, View convertView, ViewGroup parent) {

            String timerTitle;
            View vi = convertView;
            ViewHolder holder;
            String days_m = "";

                if (convertView == null) {
                    vi = inflater.inflate(R.layout.timer_items, parent, false);
                    holder = new ViewHolder();
//                    holder.timerRow = (LinearLayout) vi.findViewById(R.id.timerRow);
                    holder.time_tv = (TextView) vi.findViewById(R.id.time_txt);
                    holder.am_pm = (TextView) vi.findViewById(R.id.time_p);
                    holder.days_name = (TextView) vi.findViewById(R.id.days);
                    holder.ability = (ImageView) vi.findViewById(R.id.switches);
                    vi.setTag(holder);
                } else {
                    holder = (ViewHolder) vi.getTag();
                }

                int timerIdx = position * 2 + 2;
                if ((position % 2) == 0) {
//                    holder.timerRow.setBackgroundResource(bar_blue2);
//                    timerTitle = res.getString(R.string.s_timerOn).concat(Integer.toString((position / 2) + 1));
                    holder.ability.setImageResource(R.drawable.switch_on);
                } else {
//                    holder.timerRow.setBackgroundResource(bar_black2);
//                    timerTitle = res.getString(R.string.s_timerOff).concat(Integer.toString((position / 2) + 1));
                    holder.ability.setImageResource(R.drawable.switch_off);
                }
//                timerTitle = timerTitle.concat(res.getString(R.string.s_timerSep));
                switch (timers[timerIdx]) {
                    case Constants.TIMER_DISABLED:
                        days_m = days_m +", "+res.getString(R.string.s_disabled);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_disabled));
                        break;
                    case Constants.TIMER_DAILY:
                        days_m = days_m +", "+res.getString(R.string.s_daily);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_daily));
                        break;
                    case Constants.TIMER_SUNDAY:
                        days_m = days_m +", "+res.getString(R.string.s_sunday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_sunday));
                        break;
                    case Constants.TIMER_MONDAY:
                        days_m = days_m +", "+res.getString(R.string.s_monday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_monday));
                        break;
                    case Constants.TIMER_TUESDAY:
                        days_m = days_m +", "+res.getString(R.string.s_tuesday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_tuesday));
                        break;
                    case Constants.TIMER_WEDNESDAY:
                        days_m = days_m +", "+res.getString(R.string.s_wednesday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_wednesday));
                        break;
                    case Constants.TIMER_THURSDAY:
                        days_m = days_m +", "+res.getString(R.string.s_thursday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_thursday));
                        break;
                    case Constants.TIMER_FRIDAY:
                        days_m = days_m +", "+res.getString(R.string.s_friday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_friday));
                        break;
                    case Constants.TIMER_SATURDAY:
                        days_m = days_m +", "+res.getString(R.string.s_saturday);
//                        timerTitle = timerTitle.concat(res.getString(R.string.s_saturday));
                        break;
                }
                holder.days_name.setText(days_m);
//                if (timers[timerIdx] != Constants.TIMER_DISABLED) {
//                    timerTitle = timerTitle.concat(res.getString(R.string.s_timerSep));
//                    int hours = timers[timerIdx + 1] / 60;
//                    int mins = timers[timerIdx + 1] % 60;
//                    timerTitle = timerTitle.concat(String.format("%02d:%02d", hours, mins));
//                }
//                holder.timerName.setText(timerTitle);
                vi.setOnClickListener(new OnItemClickListener(position));
            return vi;
        }

        private class OnItemClickListener implements View.OnClickListener {
            private int oPos;

            OnItemClickListener(int pos) {
                oPos = pos;
            }

            @Override
            public void onClick(View arg0) {
                Log.i(TAG, "Click " + oPos);
                Intent intent = new Intent(MTimerEntryActivity.this, MChangeTimerActivity.class);
                Bundle b = new Bundle();
                b.putInt("Pos", oPos);
                int timer[] = new int[2];
                timerIdx = (oPos * 2) + 2;
                timer[0] = timers[timerIdx];
                timer[1] = timers[timerIdx + 1];
                b.putIntArray("Timer", timer);
                intent.putExtras(b);
                startActivityForResult(intent, TIMER_CHANGE_ACTIVITY);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TIMER_CHANGE_ACTIVITY:
                Bundle b = data.getExtras();
                int timer[] = b.getIntArray("Timer");
                if (timer != null) {
                    timers[timerIdx] = timer[0];
                    timers[timerIdx + 1] = timer[1];
                }
                timerEntryAdapter.notifyDataSetChanged();
                break;
        }

    }
}

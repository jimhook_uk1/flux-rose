package rdteq.mooklight;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

//import com.crashlytics.android.Crashlytics;
//import io.fabric.sdk.android.Fabric;

import rdteq.mooklight.MActivities.MMainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        waitToStart();
    }

    private void waitToStart(){
        new Timer().schedule(new TimerTask(){
            public void run() {
                SplashActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                            Intent intent = new Intent(SplashActivity.this, MMainActivity.class);
                            startActivity(intent);
                        SplashActivity.this.finish();
                    }
                });
            }
        }, 2000);
    }
}

/**
 * *************************************************************************************************
 * Name: Change Device Name
 * Workfile: ChangeNameActivity.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Original. Updated Header
 * 12/12/15 | RDTek | Removd unused import.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChangeNameActivity extends ActionBarActivity {
    static final String TAG = "NameChge";

    private EditText deviceNameText;
    private String devName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) Log.i(TAG, "onCreate");

        setContentView(R.layout.activity_change_name_m);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        deviceNameText = (EditText) findViewById(R.id.devNameEdit);
        devName = b.getString("DevName");
        deviceNameText.setText(devName);

        Button okButton = (Button) findViewById(R.id.devNameDone);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitActivity();
            }
        });

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                exitActivity();
//        }
//        return true;
//    }

//    @Override
//    public void onBackPressed() {
//        exitActivity();
//        super.onBackPressed();
//    }

    void exitActivity() {
        String newName = deviceNameText.getText().toString();
        if (!newName.equals(devName)) {
            if (newName.length() > 32) {
                newName = newName.substring(0, 32);
            }
            devName = newName;
        }
        Bundle b = new Bundle();
        b.putString("DevName", devName);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
    }
}

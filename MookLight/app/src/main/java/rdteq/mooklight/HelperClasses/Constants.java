/**
 * *************************************************************************************************
 * Name: Constants
 * Workfile: Constants.java
 * Author: RDTek
 * Date: 07/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Original. (Updated Header)
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

public class Constants {
    /* Communications */
    public static final int UNKNOWN_HOST_EXCEPTION = 0;
    public static final int SOCKET_INIT_IO_EXCEPTION = 1;
    public static final int STREAM_INIT_IO_EXCEPTION = 2;
    public static final int READ_IO_EXCEPTION = 3;
    public static final int WRITE_IO_EXCEPTION = 4;
    public static final int LOST_CONNECTION = 5;
    public static final int UNRECOGNIZED_RESPONSE = 6;
    public static final int CONNECT_FAULT = 7;

    /* Timers */
    public static final int TIMER_DISABLED = 0xFFFF;
    public static final int TIMER_DAILY = 7;
    public static final int TIMER_SUNDAY = 0;
    public static final int TIMER_MONDAY = 1;
    public static final int TIMER_TUESDAY = 2;
    public static final int TIMER_WEDNESDAY = 3;
    public static final int TIMER_THURSDAY = 4;
    public static final int TIMER_FRIDAY = 5;
    public static final int TIMER_SATURDAY = 6;
}

/**
 * *************************************************************************************************
 * Name: Device Name Adapter
 * Workfile: DevNameAdapter.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 04/10/15 | RDTek | Original.
 * 12/12/15 | RDTek | Changed onConnectDeviceRequest and onItemClickListener to use position rather
 *          |       | than resultant device.
 *          |       | Added remove device functionality. Changed inflater parameter from null to
 *          |       | parent, false
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rdteq.mooklight.BuildConfig;
import rdteq.mooklight.R;

import static rdteq.mooklight.R.drawable.bar_black2;
import static rdteq.mooklight.R.drawable.bar_blue2;

public class DevNameAdapter extends BaseAdapter {

    private static final String TAG = "DNAdpter";

    public static final int EDIT_DEV_NAME_MODE = 0;
    public static final int CONTROL_LIGHT_MODE = 1;
    public static final int SET_TIMER_MODE = 2;
    public static final int ADD_DEVICE_MODE = 3;
    public static final int REMOVE_DEVICE_MODE = 4;
    //static final int NUM_MODES = 2;

    /**
     * ******** Declare Used Variables ********
     */
    private ArrayList devices;
    private ArrayList otherDevices;
    private static LayoutInflater inflater = null;
    public Resources res;
    Device device = null;
    Events eventsHandler;
    int mode;

    public interface Events {
        void onConnectDeviceRequest(int pos);
    }

    /**
     * **********  DevNameAdapter Constructor ****************
     */
    public DevNameAdapter(Activity a, ArrayList d, ArrayList d2, Resources resLocal, Events appEventsHandler) {

        /********** Take passed values **********/
        devices = d;
        otherDevices = d2;
        res = resLocal;
        eventsHandler = appEventsHandler;

        /***********  Layout inflater to call external xml layout () ***********/
        inflater = (LayoutInflater) a.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /**
     * ***** What is the size of Passed Arraylist Size ***********
     */
    public int getCount() {
        if (mode == ADD_DEVICE_MODE) {
            if (otherDevices.size() <= 0) {
                return 1;
            } else {
                return otherDevices.size();
            }
        } else {
            if (devices.size() <= 0) {
                return 1;
            } else {
                return devices.size();
            }
        }
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /**
     * ****** Create a holder Class to contain inflated xml file elements ********
     */
    public static class ViewHolder {
        public TextView devName;
        public LinearLayout row;
    }

    public void setMode(int appMode) {
        mode = appMode;
    }

    /**
     * *** Depends upon data size called for each row , Create each ListView row ****
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            /****** Inflate user_item.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.row_edit_text, parent, false);

            /****** View Holder Object to contain user_item.xml file elements ******/

            holder = new ViewHolder();
            holder.devName = (TextView) vi.findViewById(R.id.devName);
            holder.row = (LinearLayout) vi.findViewById(R.id.devNameRow);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
            //Log.i(TAG,"Null " + position);
        } else {
            holder = (ViewHolder) vi.getTag();
            //Log.i(TAG,"Exists " + position);
        }

        if (mode == ADD_DEVICE_MODE) {
            if (otherDevices.size() <= 0) {
                holder.devName.setText(R.string.s_noDevice);
                holder.row.setBackgroundResource(bar_black2);
            } else {
                /***** Get each Model object from ArrayList ********/
                if (BuildConfig.DEBUG) {
                    if (position == 0) {
                        Log.d(TAG, "Refresh");
                    }
                }
                device = null;
                device = (Device) otherDevices.get(position);

                /************  Set Model values in Holder elements ***********/
                holder.devName.setText(String.format(res.getString(R.string.s_addDeviceItem), device.devName));
                holder.row.setBackgroundResource(bar_blue2);
                vi.setOnClickListener(new OnItemClickListener(position));
            }

        } else {
            if (devices.size() <= 0) {
                holder.devName.setText(R.string.s_noDevice);
                holder.row.setBackgroundResource(bar_black2);
            } else {
                /***** Get each Model object from ArrayList ********/
                if (BuildConfig.DEBUG) {
                    if (position == 0) {
                        Log.d(TAG, "Refresh");
                    }
                }
                device = null;
                device = (Device) devices.get(position);

                /************  Set Model values in Holder elements ***********/
                if (device.onLine) {
                    switch (mode) {
                        case EDIT_DEV_NAME_MODE:
                            holder.row.setBackgroundResource(bar_blue2);
                            holder.devName.setText(String.format(res.getString(R.string.s_editDevNameItem), device.devName));
                            break;
                        case CONTROL_LIGHT_MODE:
                            holder.row.setBackgroundResource(bar_blue2);
                            holder.devName.setText(String.format(res.getString(R.string.s_controlLightItem), device.devName));
                            break;
                        case SET_TIMER_MODE:
                            holder.row.setBackgroundResource(bar_blue2);
                            holder.devName.setText(String.format(res.getString(R.string.s_setTimerItem), device.devName));
                            break;
                        case REMOVE_DEVICE_MODE:
                            holder.row.setBackgroundResource(bar_blue2);
                            holder.devName.setText(String.format(res.getString(R.string.s_removeDeviceItem), device.devName));
                            break;
                        default:
                            holder.row.setBackgroundResource(bar_blue2);
                            holder.devName.setText(String.format(res.getString(R.string.s_unknownModeItem), device.devName));
                            break;
                    }
                    vi.setOnClickListener(new OnItemClickListener(position));
                } else {
                    switch (mode) {
                        case EDIT_DEV_NAME_MODE:
                            holder.row.setBackgroundResource(bar_black2);
                            holder.devName.setText(String.format(res.getString(R.string.s_editDevNameItem), device.devName));
                            break;
                        case CONTROL_LIGHT_MODE:
                            holder.row.setBackgroundResource(bar_black2);
                            holder.devName.setText(String.format(res.getString(R.string.s_controlLightItem), device.devName));
                            break;
                        case SET_TIMER_MODE:
                            holder.row.setBackgroundResource(bar_black2);
                            holder.devName.setText(String.format(res.getString(R.string.s_setTimerItem), device.devName));
                            break;
                        case REMOVE_DEVICE_MODE:
                            holder.row.setBackgroundResource(bar_black2);
                            holder.devName.setText(String.format(res.getString(R.string.s_removeDeviceItem), device.devName));
                            break;
                        default:
                            holder.row.setBackgroundResource(bar_black2);
                            holder.devName.setText(String.format(res.getString(R.string.s_unknownModeItem), device.devName));
                            break;
                    }
                }
            }
        }
        return vi;
    }

    /**
     * ****** Called when Item click in ListView ***********
     */
    private class OnItemClickListener implements View.OnClickListener {
        private int position;

        OnItemClickListener(int pos) {
            position = pos;
        }

        @Override
        public void onClick(View arg0) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Click" + position);
            eventsHandler.onConnectDeviceRequest(position);
        }
    }

}

/**
 * *************************************************************************************************
 * Name: Device Communications
 * Workfile: DeviceComms.java
 * Author: RDTek
 * Date: 07/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 25/10/15 | RDTek | Created.
 * 07/12/15 | RDTek | Tidied up.
 *          |       | Modified for persistent comms. Added new interfaces to merge register
 *          |       | into one transaction.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;

public class DeviceComms {
    // Transaction Ids
    public static final int XFER_ERROR = 0;
    public static final int CONNECTED = 1;
    public static final int ABORTED = 2;
    public static final int TID_READ_DEVNAME = 101;
    public static final int TID_READ_DEVTYPE = 102;
    public static final int TID_WRITE_DEVNAME = 103;
    public static final int TID_STORE_DEVNAME = 104;
    public static final int TID_READ_DIM = 105;
    public static final int TID_READ_ONOFF = 106;
    public static final int TID_READ_LIGHT = 107;
    public static final int TID_WRITE_DIM = 108;
    public static final int TID_WRITE_ONOFF = 109;
    public static final int TID_READ_TIMERS1 = 110;
    public static final int TID_WRITE_TIMERS1 = 111;
    public static final int TID_READ_TIMERS2 = 112;
    public static final int TID_WRITE_TIMERS2 = 113;
    public static final int TID_WRITE_DIM_ONOFF = 114;

    private final String TAG = "DevComms";

    // Device Parameter Addresses
    private static final int DEVNAME_ADDRESS = 0;
    private static final int DEVTYPE_ADDRESS = 10;
    private static final int UPDATE_ADDRESS = 0x2B;
    private static final int DIM_ADDRESS = 0x002C;
    private static final int ONOFF_ADDRESS = 0x002D;
    private static final int LIGHT_ADDRESS = 0x002E;
    private static final int TIMERS1_ADDRESS = 0x0030;
    private static final int TIMERS2_ADDRESS = 0x0040;

    // Device Parameter Sizes
    //static final int DEVNAME_SIZE = 10;
    private static final int DEVNAME_SIZE = 11; // Read DevType as well
    private static final int DEVTYPE_SIZE = 1;
    private static final int DIM_SIZE = 1;
    private static final int LIGHT_SIZE = 1;
    private static final int TIMERS1_SIZE = 16;
    private static final int TIMERS2_SIZE = 14;
    private static final int ONOFF_SIZE = 1;

    private static final boolean SINE_DIMMING = false;

    private static final boolean PERSISTENT = true;

    private boolean waitingForResponse;
    private byte devName[] = new byte[DEVNAME_SIZE * 2];
    private Context context;
    private Device device;
    private Handler msgHandler;
    private int brightness;
    private int onOff;
    private ModBusMsgXCvr modBusMsgXCvr = null;
    private ModBusEvents modBusEvents;
    private WifiAPEvents wifiAPEvents;
    private WifiManager wifiManager;

    public DeviceComms(Context appContext, Handler appMsgHandler, Device appdevice) {
        context = appContext;
        msgHandler = appMsgHandler;
        device = appdevice;
        modBusEvents = new ModBusEvents();
        wifiAPEvents = new WifiAPEvents();
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    public void close() {
        if (PERSISTENT){
            if (modBusMsgXCvr != null) {
                modBusMsgXCvr.close();
            }
        }
    }

    private class ModBusEvents implements ModBusMsgXCvr.Events {

        @Override
        public void onConnected() {
            readDevName();
        }

        @Override
        public void onDisConnected() {
            //wifiManager.disconnect();
        }

        @Override
        public void onError(int errCode) {
            waitingForResponse = false;
            //wifiManager.disconnect();
            msgHandler.sendMessage(msgHandler.obtainMessage(XFER_ERROR, errCode, 0));
        }

        @Override
        public void onMsgRxd(int[] rxMsg) {
            int i, j;
            int timers[];

            waitingForResponse = false;
            switch (rxMsg[0]) {
                case TID_READ_DEVNAME:
                    j = 0;
                    for (i = 2; i < (rxMsg.length - 1); i++) {
                        devName[j] = Helpers.getLoByte(rxMsg[i]);
                        devName[j + 1] = Helpers.getHiByte(rxMsg[i]);
                        j += 2;
                    }
                    msgHandler.sendMessage(msgHandler.obtainMessage(CONNECTED, rxMsg[rxMsg.length - 1], 0));

                    //readDevType();
                    break;

                case TID_READ_DEVTYPE:
                    switch (rxMsg[2]) {
                        case Device.LIGHT_CONTROLLER:
                            msgHandler.sendMessage(msgHandler.obtainMessage(CONNECTED, rxMsg[2], 0));
                            break;
                    }
                    break;

                case TID_READ_DIM:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_READ_DIM, rxMsg[2], 0));
                    break;

                case TID_READ_ONOFF:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_READ_ONOFF, rxMsg[2], 0));
                    break;

                case TID_READ_LIGHT:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_READ_LIGHT, rxMsg[2], 0));
                    break;

                case TID_READ_TIMERS1:
                    timers = new int[rxMsg.length - 2];
                    j = 0;
                    for (i = 2; i < rxMsg.length; i++) {
                        timers[j] = rxMsg[i];
                        j++;
                    }
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_READ_TIMERS1, timers));
                    break;

                case TID_READ_TIMERS2:
                    timers = new int[rxMsg.length - 2];
                    j = 0;
                    for (i = 2; i < rxMsg.length; i++) {
                        timers[j] = rxMsg[i];
                        j++;
                    }
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_READ_TIMERS2, timers));
                    break;

                case TID_WRITE_DIM:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_WRITE_DIM, getBright(rxMsg[3]), 0));
                    break;

                case TID_WRITE_ONOFF:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_WRITE_ONOFF, rxMsg[3], 0));
                    break;

                case TID_WRITE_TIMERS1:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_WRITE_TIMERS1));
                    break;

                case TID_WRITE_TIMERS2:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_WRITE_TIMERS2));
                    break;

                case TID_WRITE_DIM_ONOFF:
                    msgHandler.sendMessage(msgHandler.obtainMessage(TID_WRITE_DIM_ONOFF));
                    break;
            }
        }
    }

    private class WifiAPEvents implements WifiAPConnector.Events {

        @Override
        public void onConnected() {
            if (PERSISTENT) {
                modBusMsgXCvr = new ModBusMsgXCvr(modBusEvents, device.ipAdd);
            } else {
                readDevName();
            }
        }

        @Override
        public void onConnectFault() {
            msgHandler.sendMessage(msgHandler.obtainMessage(XFER_ERROR, Constants.CONNECT_FAULT, 0));
        }
    }

    private void sendMsg(int txMsg[]) {
        waitingForResponse = true;
        if (PERSISTENT) {
            modBusMsgXCvr.sendMsg(txMsg);
        } else {
            new ModBusMsgXCvr(modBusEvents, device.ipAdd, txMsg);
        }
    }
    public boolean isBusy() {
        return waitingForResponse;
    }

    public void connect() {
        wifiManager.setWifiEnabled(true);
        new WifiAPConnector(context, wifiAPEvents, wifiManager, device);
    }

    public void readDevName() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_DEVNAME;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = DEVNAME_ADDRESS;
        txMsg[3] = DEVNAME_SIZE;
        sendMsg(txMsg);
    }

    public void readDevType() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_DEVTYPE;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = DEVTYPE_ADDRESS;
        txMsg[3] = DEVTYPE_SIZE;
        sendMsg(txMsg);
    }

    public void readDim() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_DIM;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = DIM_ADDRESS;
        txMsg[3] = DIM_SIZE;
        sendMsg(txMsg);
    }

    public void readLight() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_LIGHT;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = LIGHT_ADDRESS;
        txMsg[3] = LIGHT_SIZE;
        sendMsg(txMsg);
    }

    public void readOnOff() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_ONOFF;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = ONOFF_ADDRESS;
        txMsg[3] = ONOFF_SIZE;
        sendMsg(txMsg);
    }

    public void readTimers1() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_TIMERS1;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = TIMERS1_ADDRESS;
        txMsg[3] = TIMERS1_SIZE;
        sendMsg(txMsg);
    }

    public void readTimers2() {
        int txMsg[] = new int[4];
        txMsg[0] = TID_READ_TIMERS2;
        txMsg[1] = ModBusMsgXCvr.MODBUS_READ_HOLDING_REGISTERS;
        txMsg[2] = TIMERS2_ADDRESS;
        txMsg[3] = TIMERS2_SIZE;
        sendMsg(txMsg);
    }

    public void writeDim(int brightness) {
        int txMsg[] = new int[4];
        txMsg[0] = TID_WRITE_DIM;
        txMsg[1] = ModBusMsgXCvr.MODBUS_WRITE_SINGLE_REGISTER;
        txMsg[2] = DIM_ADDRESS;
        txMsg[3] = brightness;
        sendMsg(txMsg);
    }

    public void writeOnOff(int onOff) {
        int txMsg[] = new int[4];
        txMsg[0] = TID_WRITE_ONOFF;
        txMsg[1] = ModBusMsgXCvr.MODBUS_WRITE_SINGLE_REGISTER;
        txMsg[2] = ONOFF_ADDRESS;
        txMsg[3] = onOff;
        sendMsg(txMsg);
    }

    public void writeTimers1(int appTimers[]) {
        int txMsg[] = new int[appTimers.length + 3];
        txMsg[0] = TID_WRITE_TIMERS1;
        txMsg[1] = ModBusMsgXCvr.MODBUS_WRITE_MULTIPLE_REGISTERS;
        txMsg[2] = TIMERS1_ADDRESS;
        int j = 3;
        for (int appTimer : appTimers) {
            txMsg[j] = appTimer;
            j++;
        }
        sendMsg(txMsg);
    }

    public void writeTimers2(int appTimers[]) {
        int txMsg[] = new int[appTimers.length + 3];
        txMsg[0] = TID_WRITE_TIMERS2;
        txMsg[1] = ModBusMsgXCvr.MODBUS_WRITE_MULTIPLE_REGISTERS;
        txMsg[2] = TIMERS2_ADDRESS;
        int j = 3;
        for (int appTimer : appTimers) {
            txMsg[j] = appTimer;
            j++;
        }
        sendMsg(txMsg);
    }

    public void writeDimOnOff(int brightness, int onoff) {
        int txMsg[] = new int[5];
        txMsg[0] = TID_WRITE_DIM_ONOFF;
        txMsg[1] = ModBusMsgXCvr.MODBUS_WRITE_MULTIPLE_REGISTERS;
        txMsg[2] = DIM_ADDRESS;
        txMsg[3] = brightness;
        txMsg[4] = onoff;
        sendMsg(txMsg);
    }
    private int getBright(int dim) {
        int ibright;
        if (SINE_DIMMING) {
            double ddim = (double) dim / 100;
            double bright = 20000 * java.lang.Math.asin(ddim) / java.lang.Math.PI;
            ibright = (int) bright;
        } else {
            //ibright = (dim - 10) * 190 + 2000;
            ibright = dim;
        }
        return ibright;
    }

    private int getDim(int value) {
        int ivalue;
        if (SINE_DIMMING) {
            double dvalue = java.lang.Math.round(100 * java.lang.Math.sin(value * java.lang.Math.PI / 20000));
            ivalue = (int) dvalue;
        } else {
            //ivalue = ((value - 2000) / 190) + 10;
            ivalue = value;
        }
        return ivalue;
    }
}
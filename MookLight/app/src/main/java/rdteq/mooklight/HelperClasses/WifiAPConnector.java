/**
 * *************************************************************************************************
 * Name: Wifi Connector
 * Workfile: WifiConnector.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController project.
 * 14/09/15 | RDTek | First Release
 * 07/12/15 | RDTek | Tidied up. Modified connection thread.
 * 12/12/15 | RDTek | Replaced Thread functionality with BroadCastReceiver
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import rdteq.mooklight.BuildConfig;

public class WifiAPConnector {
    private static final String TAG = "WifiAP__";

    private static final int STATE_NOT_CONNECTING = 0;
    private static final int STATE_WAITING_CONNECT = 1;
    private static final int STATE_CONNECTING = 2;
    private static final int STATE_CONNECTED = 3;

    private static final int CONNECT_TIMEOUT_COUNT = 200; /* Timeout = CONNECT_TIMEOUT_COUNT * THREAD_SLEEP_TIME msecs */
    private static final int DISCONNECT_TIMEOUT_COUNT = 200; /* Timeout = DISCONNECT_TIMEOUT_COUNT * THREAD_SLEEP_TIME msecs */
    private static final int POST_CONNECT_DELAY_TIME = 10; /* Delay = POST_CONNECT_DELAY_TIME * THREAD_SLEEP_TIME msecs */
    private static final int THREAD_SLEEP_TIME = 100;

    private ConnectBroadcastReceiver connectBR;
    int connectingState;
    private ConnectivityManager connectivityManager;
    private Context context;
    private Events eventsHandler;
    private int networkId;
    private String ssidWithQuotes;
    private WifiAP wifiAP;
    private WifiManager wifiManager;

    public interface Events {
        void onConnected();

        void onConnectFault();
    }

    public WifiAPConnector(Context appContext, Events appEventsHandler, WifiManager appWifiManager,
                           WifiAP appWifiAP) {
        int i;
        boolean found;
        List<WifiConfiguration> attachedAps;

        eventsHandler = appEventsHandler;
        wifiManager = appWifiManager;
        wifiAP = appWifiAP;
        context = appContext;

        connectivityManager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check if we're trying to connect to a registered network
        attachedAps = wifiManager.getConfiguredNetworks();
        i = 0;
        found = false;
        ssidWithQuotes = "\"" + wifiAP.SSID + "\"";
        if (BuildConfig.DEBUG) Log.i(TAG, "Connecting to: " + wifiAP.SSID);
        while ((i < attachedAps.size()) &&
                !found) {
            if (ssidWithQuotes.contentEquals(attachedAps.get(i).SSID)) {
                found = true;
            } else {
                i++;
            }
        }

        if (found) {
            // Registered network
            connectAP(appContext, attachedAps.get(i).networkId);
        } else {
            eventsHandler.onConnectFault();
        }
    }

    private void connectAP(Context context, int newNetworkId) {
        connectingState = STATE_NOT_CONNECTING;
        if (newNetworkId >= 0) {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo.isConnected()) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if (BuildConfig.DEBUG) Log.i(TAG, "Connected to: " + wifiInfo.getSSID());
                if ((!wifiInfo.getSSID().equals(wifiAP.SSID) &&
                        (!wifiInfo.getSSID().equals(ssidWithQuotes)))) {
                    if (BuildConfig.DEBUG) Log.i(TAG, "Connecting to different AP");
                    connectingState = STATE_WAITING_CONNECT;
                } else {
                    if (BuildConfig.DEBUG) Log.i(TAG, "Already Connected");
                    connectingState = STATE_CONNECTED;
                }
            } else {
                if (BuildConfig.DEBUG) Log.i(TAG, "Connecting to AP");
                connectingState = STATE_WAITING_CONNECT;
            }
        } else {
            if (BuildConfig.DEBUG) Log.e(TAG, "Unconfigured Network");
        }
        switch (connectingState) {
            case STATE_NOT_CONNECTING:
                eventsHandler.onConnectFault();
                break;
            case STATE_WAITING_CONNECT:
                networkId = newNetworkId;
                if (wifiManager.enableNetwork(networkId, true)) {
                    connectBR = new ConnectBroadcastReceiver();
                    context.registerReceiver(connectBR, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
                } else {
                    eventsHandler.onConnectFault();
                }
                //ConnectThread connectThread = new ConnectThread();
                //connectThread.setName("Connect Thread");
                //connectThread.start();
                if (BuildConfig.DEBUG) Log.i(TAG, "ConnectThread started");
                break;
            case STATE_CONNECTED:
                eventsHandler.onConnected();
                break;
            default:
                eventsHandler.onConnectFault();
                break;
        }
    }

    private class ConnectBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BuildConfig.DEBUG) Log.i(TAG, "BR:" + intent.getAction());
            Bundle extras = intent.getExtras();
            NetworkInfo info = extras.getParcelable("networkInfo");
            NetworkInfo.State connectInfo = info.getState();
            Log.i(TAG, "BR: Network State: " + connectInfo);
            if (connectInfo == NetworkInfo.State.CONNECTING) {
                connectingState = STATE_CONNECTING;
            } else if (connectInfo == NetworkInfo.State.CONNECTED) {
                if (connectingState == STATE_CONNECTING) {
                    if (BuildConfig.DEBUG)
                        Log.i(TAG, "Connected :-) " + wifiManager.getConnectionInfo().getIpAddress());
                    connectingState = STATE_CONNECTED;
                    context.unregisterReceiver(connectBR);
                    eventsHandler.onConnected();
                }
            }
        }
    }

    private class ConnectThread extends Thread {
        NetworkInfo networkInfo;
        SupplicantState supplicantState;
        boolean threadRunning;
        int waitTime;

        @Override
        public void run() {
            super.run();

            try {
                threadRunning = true;

                    /*Initiate connection to new AP */
                if (wifiManager.enableNetwork(networkId, true)) {
                    waitTime = 0;
                    networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    supplicantState = wifiManager.getConnectionInfo().getSupplicantState();
                    if (BuildConfig.DEBUG) {
                        Log.i(TAG, "Network State: " + networkInfo.getState());
                        Log.i(TAG, "Supp State: " + supplicantState);
                    }
                    while (networkInfo.isConnected() && threadRunning) {
                        networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                        supplicantState = wifiManager.getConnectionInfo().getSupplicantState();
                        if (BuildConfig.DEBUG) {
                            Log.i(TAG, "Network State: " + networkInfo.getState());
                            Log.i(TAG, "Supp State: " + supplicantState);
                        }
                        waitTime++;
                        if (waitTime == DISCONNECT_TIMEOUT_COUNT) {
                            if (BuildConfig.DEBUG) Log.e(TAG, "Not Connected :-(");
                            eventsHandler.onConnectFault();
                            threadRunning = false;
                        } else {
                            Thread.sleep(THREAD_SLEEP_TIME);
                        }
                    }

                        /* If we disconnected successfully wait for connection to new AP */
                    while (!networkInfo.isConnected() && threadRunning) {
                        networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                        supplicantState = wifiManager.getConnectionInfo().getSupplicantState();
                        if (BuildConfig.DEBUG) {
                            Log.i(TAG, "Network State: " + networkInfo.getState());
                            Log.i(TAG, "Supp State: " + supplicantState);
                        }
                        waitTime++;
                        if (waitTime == CONNECT_TIMEOUT_COUNT) {
                            if (BuildConfig.DEBUG) Log.e(TAG, "Not Connected :-(");
                            eventsHandler.onConnectFault();
                            threadRunning = false;
                        } else {
                            Thread.sleep(THREAD_SLEEP_TIME);
                        }
                    }

                    waitTime = 0;
                    while ((waitTime != POST_CONNECT_DELAY_TIME) && threadRunning) {
                        waitTime++;
                        Thread.sleep(THREAD_SLEEP_TIME);
                    }


                        /* If we've connected to an AP check that we've connected to the intended AP */
                    if (threadRunning) {
                        if (networkInfo.isConnected()) {
                            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                            if (BuildConfig.DEBUG)
                                Log.i(TAG, "Connected to: " + wifiInfo.getSSID());
                            if ((!wifiInfo.getSSID().equals(wifiAP.SSID) &&
                                    (!wifiInfo.getSSID().equals(ssidWithQuotes)))) {
                                if (BuildConfig.DEBUG) Log.i(TAG, "Not Connected :-(");
                                eventsHandler.onConnectFault();
                            } else {
                                if (BuildConfig.DEBUG)
                                    Log.i(TAG, "Connected :-) " + wifiManager.getConnectionInfo().getIpAddress());
                                context.unregisterReceiver(connectBR);
                                eventsHandler.onConnected();
                            }
                        } else {
                            if (BuildConfig.DEBUG) Log.e(TAG, "Not Connected :-(");
                            eventsHandler.onConnectFault();
                        }
                        threadRunning = false;
                    }
                } else {
                    if (BuildConfig.DEBUG) Log.e(TAG, "Not Connected :-(");
                    eventsHandler.onConnectFault();
                }
            } catch (InterruptedException ie) {
                threadRunning = false;
            }

        }
    }
}
/**
 * *************************************************************************************************
 * Name: WifiAP
 * Workfile: WifiAP.java
 * Author: RDTek
 * Date: 14/09/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController project.
 * 14/09/15 | RDTek | First Release
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

public class WifiAP {
    public static final int OPEN_AP = 0;
    public static final int WEP_AP = 1;
    public static final int WPA_AP = 2;

    public int apType;
    public String BSSID;
    public String SSID;

    public WifiAP() {
    }
}
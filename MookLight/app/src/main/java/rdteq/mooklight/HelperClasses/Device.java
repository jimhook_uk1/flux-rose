/**
 * *************************************************************************************************
 * Name: Device
 * Workfile: Device.java
 * Author: RDTek
 * Date: 14/09/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController project.
 * 14/09/15 | RDTek | First Release
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

public class Device extends WifiAP {

    public static final int UNKNOWN_DEVICE = 0;
    public static final int LIGHT_CONTROLLER = 1;

    public boolean onLine;
    public String ipAdd;
    public int devType;
    public String devName;
    public int brightness;
    public int onoff;
    public int lightOn;
    public boolean onMyNetwork;
}

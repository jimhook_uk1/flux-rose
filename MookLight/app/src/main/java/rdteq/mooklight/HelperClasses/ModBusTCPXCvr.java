/**
 * *************************************************************************************************
 * Name: Modbus TCP Trasnceiver
 * Workfile: ModBusTCPXCvr.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 24/10/15 | RDTek | Original.
 * 07/12/15 | RDTek | Tidied Up. Modified for persistent communications
 * 12/12/15 | RDTek | Added thread names.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.util.Log;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import rdteq.mooklight.BuildConfig;

public class ModBusTCPXCvr {
    private final String TAG = "MBXCvr__";

    private static final int CONNECT_TIEMOUT = 10000; /* msecs */
    private static final int RECEIVE_MSG_SIZE = 64;
    private static final int RECEIVE_TIMEOUT = 10000; /* msecs */
    private static final int SOCKET_CONNECT_RETRY_LIMIT = 3;
    private static final int SERVERPORT = 502;

    private boolean persistent;
    private boolean txRdy;
    private byte txMsg[];
    private Events eventsHandler;
    private InputStream in;
    private OutputStream out;
    private Socket socket;
    private boolean threadRunning;

    public interface Events {
        void onConnected();

        void onDisConnected();

        void onError(int errCode);

        void onMsgRxd(int id, byte rxMsg[]);
    }

    /**
     * Initialises vars and opens TCP socket.
     *
     * @param appEventsHandler Reference for event handlers
     * @param targetIP         IP address of device to connect to
     */
    public ModBusTCPXCvr(Events appEventsHandler, String targetIP) {
        eventsHandler = appEventsHandler;
        persistent = true;
        OpenSocket openSocket = new OpenSocket(targetIP);
        openSocket.setName("Open Socket Thread");
        openSocket.start();
    }

    public ModBusTCPXCvr(Events appEventsHandler, String targetIP, byte msg[], int transactionId) {
        eventsHandler = appEventsHandler;
        persistent = false;
        makeTxMsg(msg, transactionId);
        OpenSocket openSocket = new OpenSocket(targetIP);
        openSocket.start();
    }

    public void close() {
        threadRunning = false;
    }

    public void sendMsg(byte msg[], int transactionId) {
        makeTxMsg(msg, transactionId);
        txRdy = true;
    }

    /**
     * @param msg           text entered by client
     * @param transactionId transaction identifier
     */
    private void makeTxMsg(byte msg[], int transactionId) {
        int i, j, s;
        int size = msg.length;
        s = size + 1;
        txMsg = new byte[size + 7];
        txMsg[0] = (byte) (transactionId >> 8);
        txMsg[1] = (byte) transactionId;
        txMsg[2] = 0;
        txMsg[3] = 0;
        txMsg[4] = (byte) (s >> 8);
        txMsg[5] = (byte) s;
        txMsg[6] = 0;
        j = 7;
        for (i = 0; i < size; i++) {
            txMsg[j] = msg[i];
            j++;
        }
    }

    private void closeTCP() {
        try {
            if (in != null) {
                socket.shutdownInput();
                in = null;
            }
            if (out != null) {
                socket.shutdownOutput();
                out = null;
            }
            if (socket != null) {
                socket.close();
                socket = null;
                if (BuildConfig.DEBUG) Log.i(TAG, "Socket Closed");
            }
        } catch (java.io.IOException ioe) {
            if (BuildConfig.DEBUG) Log.e(TAG, "Error closing stream/socket: " + ioe);
        }
    }

    private class OpenSocket extends Thread {
        String ipAddress;

        public OpenSocket(String targetIP) {
            ipAddress = targetIP;
        }

        public void run() {

            InetAddress serverAddr = null;
            boolean error = false;
            try {
                serverAddr = InetAddress.getByName(ipAddress);
            } catch (UnknownHostException uhe) {
                if (BuildConfig.DEBUG) Log.e(TAG, "Unknown Host Exception", uhe);
                error = true;
                eventsHandler.onError(Constants.UNKNOWN_HOST_EXCEPTION);
            }

            /*
                Try to connect:
                This is repeated a number of times in case server isn't ready
             */
            if (!error) {
                int retry = 0;
                error = true;
                while ((retry != SOCKET_CONNECT_RETRY_LIMIT) && error) {
                    try {
                        //create a socket to make the connection with the server
                        socket = new Socket();
                        socket.setSoTimeout(RECEIVE_TIMEOUT);
                        if (retry != 0) {
                            sleep(500);
                        }
                        socket.connect(new InetSocketAddress(serverAddr, SERVERPORT), CONNECT_TIEMOUT);
                        //socket.connect(new InetSocketAddress(serverAddr, SERVERPORT));
                        error = false;
                    } catch (Exception e) {
                        retry++;
                        if (retry == SOCKET_CONNECT_RETRY_LIMIT) {
                            if (BuildConfig.DEBUG) Log.e(TAG, "Socket Init Error", e);
                            eventsHandler.onError(Constants.SOCKET_INIT_IO_EXCEPTION);
                        } else {
                            closeTCP();
                            error = true;
                        }
                    }
                }
            }
            if (!error) {
                try {
                    //send the message to the server
                    out = socket.getOutputStream();

                    //receive the message which the server sends back
                    in = socket.getInputStream();

                    if (BuildConfig.DEBUG) Log.i(TAG, "Socket Open");
                    //eventsHandler.onConnected();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) Log.i(TAG, "Stream Init Error");
                    closeTCP();
                    eventsHandler.onError(Constants.STREAM_INIT_IO_EXCEPTION);
                }
            }
            if (!error) {
                if (!persistent) {
                    TCPReceiver tcpReceiver = new TCPReceiver();
                    tcpReceiver.start();
                    try {
                        out.write(txMsg, 0, txMsg.length);
                        out.flush();
                        if (BuildConfig.DEBUG) Log.i(TAG, "Sent Message:" + txMsg[1]);
                    } catch (Exception e) {
                        if (BuildConfig.DEBUG) Log.e(TAG, "Send Message Failed: ", e);
                        closeTCP();
                        eventsHandler.onError(Constants.WRITE_IO_EXCEPTION);
                    }
                } else {
                    TCPTransceiver tcpTRansceiver = new TCPTransceiver();
                    tcpTRansceiver.setName("TCP Transceiver Thread");
                    tcpTRansceiver.start();
                    eventsHandler.onConnected();
                }
            }
        }
    }

    private class TCPReceiver extends Thread {

        public void run() {
            byte rxMsg[] = new byte[RECEIVE_MSG_SIZE];
            byte serverMsg[];
            int i, j;
            int size;
            int transactionId;
            if (BuildConfig.DEBUG) Log.i(TAG, "Receiver Running");
            //in this while the client listens for the messages sent by the server
            try {
                size = in.read(rxMsg);
                if (size > 0) {
                    if (BuildConfig.DEBUG) Log.i(TAG, "Received Message:");
                    if ((rxMsg[0] == txMsg[0]) &&
                            (rxMsg[1] == txMsg[1])) {
                                    /*
                                     Transaction ID of received msg matches transmitted msg so
                                     remove header and pass back up a level
                                      */
                        serverMsg = new byte[size];
                        j = 0;
                        for (i = 7; i < size; i++) {
                            serverMsg[j] = rxMsg[i];
                            j++;
                        }
                        if (BuildConfig.DEBUG) Log.i(TAG, "Message Accepted " + rxMsg[1] + j);
                        transactionId = ((rxMsg[0] & 0xFF) << 8) + (rxMsg[1] & 0xFF);
                        closeTCP();
                        eventsHandler.onMsgRxd(transactionId, serverMsg);
                    } else {
                        if (BuildConfig.DEBUG) Log.i(TAG, "Received Unrecognized Message:");
                        eventsHandler.onError(Constants.UNRECOGNIZED_RESPONSE);
                    }
                } else {
                    if (BuildConfig.DEBUG) Log.i(TAG, "Server Disconnected");

                    closeTCP();
                    eventsHandler.onError(Constants.LOST_CONNECTION);
                }
            } catch (InterruptedIOException iioe) {
                if (BuildConfig.DEBUG) Log.i(TAG, "Timeout");
                closeTCP();
                eventsHandler.onError(Constants.LOST_CONNECTION);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) Log.e(TAG, "Stream Error", e);
                closeTCP();
                eventsHandler.onError(Constants.READ_IO_EXCEPTION);
            }
            if (BuildConfig.DEBUG) Log.i(TAG, "TCP Client Finished");
        }
    }

    private class TCPTransceiver extends Thread {
        private boolean waitingResponse;

        public void run() {
            byte rxMsg[] = new byte[RECEIVE_MSG_SIZE];
            byte serverMsg[];
            int i, j;
            int size;
            int transactionId;
            if (BuildConfig.DEBUG) Log.i(TAG, "Transceiver Running");
            threadRunning = true;
            //in this while the client listens for the messages sent by the server
            try {
                while (threadRunning) {
                    if (waitingResponse) {
                        size = in.read(rxMsg);
                        if (size > 0) {
                            if (BuildConfig.DEBUG) Log.i(TAG, "Received Message:");
                            if ((rxMsg[0] == txMsg[0]) &&
                                    (rxMsg[1] == txMsg[1])) {
                                    /*
                                     Transaction ID of received msg matches transmitted msg so
                                     remove header and pass back up a level
                                      */
                                serverMsg = new byte[size];
                                j = 0;
                                for (i = 7; i < size; i++) {
                                    serverMsg[j] = rxMsg[i];
                                    j++;
                                }
                                if (BuildConfig.DEBUG)
                                    Log.i(TAG, "Message Accepted " + rxMsg[1] + j);
                                transactionId = ((rxMsg[0] & 0xFF) << 8) + (rxMsg[1] & 0xFF);
                                waitingResponse = false;
                                eventsHandler.onMsgRxd(transactionId, serverMsg);
                            } else {
                                if (BuildConfig.DEBUG)
                                    Log.i(TAG, "Received Unrecognized Message:");
                                eventsHandler.onError(Constants.UNRECOGNIZED_RESPONSE);
                            }
                        } else {
                            if (BuildConfig.DEBUG) Log.i(TAG, "Server Disconnected");
                            threadRunning = false;
                            eventsHandler.onError(Constants.LOST_CONNECTION);
                        }
                    } else if (txRdy) {
                        out.write(txMsg, 0, txMsg.length);
                        out.flush();
                        if (BuildConfig.DEBUG) Log.i(TAG, "Sent Message:" + txMsg[1]);
                        txRdy = false;
                        waitingResponse = true;
                    }
                }
            } catch (InterruptedIOException iioe) {
                if (BuildConfig.DEBUG) Log.i(TAG, "Timeout");
                eventsHandler.onError(Constants.LOST_CONNECTION);
            } catch (Exception e) {
                if (BuildConfig.DEBUG) Log.e(TAG, "Stream Error", e);
                eventsHandler.onError(Constants.READ_IO_EXCEPTION);
            } finally {
                if (BuildConfig.DEBUG) Log.i(TAG, "TCP Client Finished");
                closeTCP();
                eventsHandler.onDisConnected();
            }
        }

    }
}
/**
 * *************************************************************************************************
 * Name: Wifi Scanner
 * Workfile: WifiScanner.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController project.
 * 14/09/15 | RDTek | First Release
 * 12/12/15 | RDTek | Modified so that scanner and broadcast receiver are stopped when stop()
 *          |       | is called. Changed Thread to a TimerTask.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.app.Activity;
import android.os.Build;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.Manifest;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rdteq.mooklight.BuildConfig;

public class WifiScanner {
    private static final String TAG = "WifiScan";

    public static final int REQUEST_ACCESS_COARSE_LOCATION = 1;

    private static final int SCAN_INTERVAL = 5000; //msecs

    private Activity activity;
    private boolean doScan;
    private Context context;
    private Events eventsHandler;
    private ScanBroadcastReceiver scanBR;
    private Timer timer;
    private WifiManager wifiManager;

    public interface Events {
        void onScanComplete(ArrayList<WifiAP> apList);
    }

    public WifiScanner(Activity appActivity, Context appContext, Events appEventsHandler, WifiManager appWifiManager) {
        activity = appActivity;
        context = appContext;
        eventsHandler = appEventsHandler;
        wifiManager = appWifiManager;
        scanBR = new ScanBroadcastReceiver();
    }

    private class ScanBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context c, Intent intent) {
            if (BuildConfig.DEBUG) Log.i(TAG, "BR:" + intent.getAction());
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_ACCESS_COARSE_LOCATION);
                } else {
                    getScanningResults();
                }
            }
        }
    }

    public void getScanningResults() {
        int i;
        int size;
        List<ScanResult> results;
        ScanResult scanResult;
        ArrayList<WifiAP> apList = new ArrayList<>();
        String capabilities;
        WifiAP wifiAP;

        results = wifiManager.getScanResults();
        size = results.size();
        i = 0;
        while (i < size) {
            scanResult = results.get(i);
            capabilities = scanResult.capabilities;
            wifiAP = new WifiAP();
            wifiAP.SSID = scanResult.SSID;
            wifiAP.BSSID = scanResult.BSSID;
            if (capabilities.contains("WPA")) {
                wifiAP.apType = WifiAP.WPA_AP;
            } else if (capabilities.contains("WEP")) {
                wifiAP.apType = WifiAP.WEP_AP;
            } else {
                wifiAP.apType = WifiAP.OPEN_AP;
            }
            apList.add(wifiAP);
            i++;
        }
        if (apList.size() != 0) {
            eventsHandler.onScanComplete(apList);
        }
        if (BuildConfig.DEBUG) Log.i(TAG, "Scan Results:" + apList.size());
        if (doScan) {
            wifiManager.startScan();
        }
     }

    public void startScan() {
        if (wifiManager.isWifiEnabled()) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Scanner started");
            doScan = true;
            context.registerReceiver(scanBR, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiManager.startScan();
            //timer = new Timer();
            //Scanner scanner = new Scanner();
            //timer.schedule(scanner, 0, SCAN_INTERVAL);
        }
    }

    public void stopScan() {
        if (doScan) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Scanner stopped");
            //timer.cancel();
            context.unregisterReceiver(scanBR);
            doScan = false;
        }
    }

    private class Scanner extends TimerTask {
        @Override
        public void run() {
            if (BuildConfig.DEBUG) Log.i(TAG, "Do Scan");
            wifiManager.startScan();
        }
    }
}
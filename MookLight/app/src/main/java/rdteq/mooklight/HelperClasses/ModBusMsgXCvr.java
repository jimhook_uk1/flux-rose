/**
 * *************************************************************************************************
 * Name: ModBus Message Transceiver
 * Workfile: ModBusMsgXCvr.java
 * Author: RDTek
 * Date: 07/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 24/10/15 | RDTek | Original.
 * 07/12/15 | RDTek | Tidied up. Modified for persistant communications
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

import android.util.Log;

import rdteq.mooklight.BuildConfig;

public class ModBusMsgXCvr {
    static final String TAG = "MBMgXCvr";

    public static final byte MODBUS_READ_HOLDING_REGISTERS = 0x03;
    public static final byte MODBUS_WRITE_SINGLE_REGISTER = 0x06;
    public static final byte MODBUS_WRITE_MULTIPLE_REGISTERS = 0x10;

    private boolean persistent;
    private boolean waitingForResponse;
    private Events eventsHandler;
    private ModBusTCPXCvr modBusTCPXCvr;
    private ModBusTCPXCvr.Events tcpEvents;
    private String targetIP;

    public ModBusMsgXCvr(Events appEventsHandler, String appTargetIP, int txmsg[]) {
        eventsHandler = appEventsHandler;
        waitingForResponse = false;
        tcpEvents = new TCPEvents();
        targetIP = appTargetIP;
        persistent = false;
        sendMsg(txmsg);
    }

    public ModBusMsgXCvr(Events appEventsHandler, String appTargetIP) {
        eventsHandler = appEventsHandler;
        waitingForResponse = false;
        tcpEvents = new TCPEvents();
        targetIP = appTargetIP;
        persistent = true;
        modBusTCPXCvr = new ModBusTCPXCvr(tcpEvents, targetIP);
    }

    public void close() {
        if (persistent) {
            modBusTCPXCvr.close();
        }
    }

    private void txMsg(byte msg[], int transactionId) {
        waitingForResponse = true;
        if (persistent) {
            modBusTCPXCvr.sendMsg(msg, transactionId);
        } else {
            new ModBusTCPXCvr(tcpEvents, targetIP, msg, transactionId);
        }
    }

    public void sendMsg(int txmsg[]) {
        /*  Handle txMsg
            0 - Transaction id
            1 - Function Code
            2..n - Parameters & data
         */
        byte msg[];
        switch (txmsg[1]) {
            case MODBUS_READ_HOLDING_REGISTERS:
                msg = new byte[5];
                msg[0] = Helpers.getLoByte(txmsg[1]); // Function Code
                msg[1] = Helpers.getHiByte(txmsg[2]); // Address
                msg[2] = Helpers.getLoByte(txmsg[2]);
                msg[3] = Helpers.getHiByte(txmsg[3]); // Num Reg
                msg[4] = Helpers.getLoByte(txmsg[3]);
                txMsg(msg, txmsg[0]);
                break;

            case MODBUS_WRITE_MULTIPLE_REGISTERS:
                int i, j;
                int numReg = txmsg.length - 3;
                msg = new byte[numReg * 2 + 6];
                msg[0] = Helpers.getLoByte(txmsg[1]); // Function Code
                msg[1] = Helpers.getHiByte(txmsg[2]); // Address
                msg[2] = Helpers.getLoByte(txmsg[2]);
                msg[3] = Helpers.getHiByte(numReg);   // Num Reg
                msg[4] = Helpers.getLoByte(numReg);
                msg[5] = Helpers.getLoByte(numReg * 2);
                j = 6;
                for (i = 3; i < txmsg.length; i++) {
                    msg[j] = Helpers.getHiByte(txmsg[i]);
                    msg[j + 1] = Helpers.getLoByte(txmsg[i]);
                    j += 2;
                }
                waitingForResponse = true;
                txMsg(msg, txmsg[0]);
                break;

            case MODBUS_WRITE_SINGLE_REGISTER:
                msg = new byte[5];
                msg[0] = Helpers.getLoByte(txmsg[1]); // Function code
                msg[1] = Helpers.getHiByte(txmsg[2]); // Address
                msg[2] = Helpers.getLoByte(txmsg[2]);
                msg[3] = Helpers.getHiByte(txmsg[3]); // Value
                msg[4] = Helpers.getLoByte(txmsg[3]);
                waitingForResponse = true;
                txMsg(msg, txmsg[0]);
                break;
            default:
                if (BuildConfig.DEBUG) Log.e(TAG, "Unknown Message");
                break;
        }
    }

    public interface Events {
        void onConnected();

        void onDisConnected();

        void onError(int errCode);

        void onMsgRxd(int rxMsg[]);
    }

    public boolean isBusy() {
        return waitingForResponse;
    }

    private class TCPEvents implements ModBusTCPXCvr.Events {

        @Override
        public void onConnected() {
            eventsHandler.onConnected();
        }

        @Override
        public void onDisConnected() {
            eventsHandler.onDisConnected();
        }

        @Override
        public void onError(int errCode) {
            eventsHandler.onError(errCode);
        }

        @Override
        public void onMsgRxd(int id, byte msg[]) {
            waitingForResponse = false;
            int rxMsg[];
            switch (msg[0]) {
                case MODBUS_READ_HOLDING_REGISTERS:
                    int size = msg[1] / 2 + 2;
                    rxMsg = new int[size];
                    rxMsg[0] = id;
                    rxMsg[1] = msg[0]; // Function Code
                    int i, j;
                    j = 2;
                    for (i = 2; i < size; i++) {
                        rxMsg[i] = Helpers.getuint16(msg, j);
                        j += 2;
                    }
                    eventsHandler.onMsgRxd(rxMsg);
                    break;

                case MODBUS_WRITE_MULTIPLE_REGISTERS:
                    rxMsg = new int[4];
                    rxMsg[0] = id;
                    rxMsg[1] = msg[0];              // Function code
                    rxMsg[2] = Helpers.getuint16(msg, 1);   // Address
                    rxMsg[3] = Helpers.getuint16(msg, 3);   // Num Regs
                    eventsHandler.onMsgRxd(rxMsg);
                    break;

                case MODBUS_WRITE_SINGLE_REGISTER:
                    rxMsg = new int[4];
                    rxMsg[0] = id;
                    rxMsg[1] = msg[0];              // Function code
                    rxMsg[2] = Helpers.getuint16(msg, 1);   // Address
                    rxMsg[3] = Helpers.getuint16(msg, 3);   // Value
                    eventsHandler.onMsgRxd(rxMsg);
                    break;
                default:
                    if (BuildConfig.DEBUG) Log.e(TAG, "Unknown Message");
                    break;
            }
        }
    }
}
/**
 * *************************************************************************************************
 * Name: Helper Functions
 * Workfile: Helpers.java
 * Author: RDTek
 * Date: 14/09/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 31/08/15 | RDTek | Copied from LightController project.
 * 14/09/15 | RDTek | First Release.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight.HelperClasses;

public class Helpers {

    /*
     * @param bytearray
     * @param offset
     * @returns unsigned short value generated from the bytearray with the msb at offset and the lsb
      * at (offset + 1)
     */
    public static int getuint16(byte barray[], int offset) {
        int value = barray[offset + 1] & 0xFF;
        value += (barray[offset] & 0xFF) << 8;
        return value;
    }

    /*
     * @param value
     * @returns most significant byte of value
     */
    public static byte getHiByte(int value) {
        return (byte) (value >> 8);
    }

    /*
     * @param value
     * @returns least significant byte of value
     */
    public static byte getLoByte(int value) {
        return (byte) value;
    }
}

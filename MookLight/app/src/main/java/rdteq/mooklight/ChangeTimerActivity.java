/**
 * *************************************************************************************************
 * Name: Change Timer
 * Workfile: ChangeTimerActivity.java
 * Author: RDTek
 * Date: 07/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Original. Updated Header
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import rdteq.mooklight.HelperClasses.Constants;

public class ChangeTimerActivity extends ActionBarActivity {
    static final String TAG = "TimeChge";

    private static final int TIMER_SPINNER_DISABLED = 0;
    private static final int TIMER_SPINNER_DAILY = 1;
    private static final int TIMER_SPINNER_SUNDAY = 2;
    private static final int TIMER_SPINNER_MONDAY = 3;
    private static final int TIMER_SPINNER_TUESDAY = 4;
    private static final int TIMER_SPINNER_WEDNESDAY = 5;
    private static final int TIMER_SPINNER_THURSDAY = 6;
    private static final int TIMER_SPINNER_FRIDAY = 7;
    private static final int TIMER_SPINNER_SATURDAY = 8;

    private int timer[] = new int[2];

    private EditText alarmTime;
    private Spinner timerSpinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        /* Get timer that we are changing */
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        int position = b.getInt("Pos");
        timer = b.getIntArray("Timer");

        /* Get resources */
        setContentView(R.layout.activity_change_timer);
        TextView title = (TextView) findViewById(R.id.changeTimerTitle);
        alarmTime = (EditText) findViewById(R.id.timeEdit);
        Button okButton = (Button) findViewById(R.id.timerDone);
        timerSpinner = (Spinner) findViewById(R.id.timerSpinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.timers_spinner_array, R.layout.timer_spinner_row);
        spinnerAdapter.setDropDownViewResource(R.layout.timer_spinner_row);

        /* Initialise Title */
        if ((position % 2) == 0) {
            title.setText(getResources().getString(R.string.s_timerOn).concat(Integer.toString((position / 2) + 1)));
        } else {
            title.setText(getResources().getString(R.string.s_timerOff).concat(Integer.toString((position / 2) + 1)));
        }

        /* Initialise Spinner */
        timerSpinner.setAdapter(spinnerAdapter);
        switch (timer[0]) {
            case Constants.TIMER_DISABLED:
                timerSpinner.setSelection(TIMER_SPINNER_DISABLED);
                break;
            case Constants.TIMER_DAILY:
                timerSpinner.setSelection(TIMER_SPINNER_DAILY);
                break;
            case Constants.TIMER_SUNDAY:
                timerSpinner.setSelection(TIMER_SPINNER_SUNDAY);
                break;
            case Constants.TIMER_MONDAY:
                timerSpinner.setSelection(TIMER_SPINNER_MONDAY);
                break;
            case Constants.TIMER_TUESDAY:
                timerSpinner.setSelection(TIMER_SPINNER_TUESDAY);
                break;
            case Constants.TIMER_WEDNESDAY:
                timerSpinner.setSelection(TIMER_SPINNER_WEDNESDAY);
                break;
            case Constants.TIMER_THURSDAY:
                timerSpinner.setSelection(TIMER_SPINNER_THURSDAY);
                break;
            case Constants.TIMER_FRIDAY:
                timerSpinner.setSelection(TIMER_SPINNER_FRIDAY);
                break;
            case Constants.TIMER_SATURDAY:
                timerSpinner.setSelection(TIMER_SPINNER_SATURDAY);
                break;
        }

        /* Initialise Time */
        if (timer[0] == Constants.TIMER_DISABLED) {
            alarmTime.setText(R.string.s_midnight);
        } else {
            int hour = timer[1] / 60;
            int minute = timer[1] % 60;
            alarmTime.setText(String.format("%02d:%02d", hour, minute));
        }

        /* Set up button */
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finishWithResult();
            }
        });

    }

    private void finishWithResult() {
        boolean timeValid;

        timeValid = false;
            String timeString = alarmTime.getText().toString();
            if (timeString.length() == 5) {
                char[] timechar = timeString.toCharArray();
                if (Character.isDigit(timechar[0]) &&
                        Character.isDigit(timechar[1]) &&
                        Character.isDigit(timechar[3]) &&
                        Character.isDigit(timechar[4]) &&
                        (timechar[2] == ':')) {
                    int hour = Character.getNumericValue(timechar[0]) * 10 +
                            Character.getNumericValue(timechar[1]);
                    int minute = Character.getNumericValue(timechar[3]) * 10 +
                            Character.getNumericValue(timechar[4]);
                    if ((hour <= 23) &&
                            (minute <= 60)) {
                        timer[1] = hour * 60 + minute;
                        timeValid = true;
                    }
                }
            }

        if (timeValid) {
            switch (timerSpinner.getSelectedItemPosition()) {
                case TIMER_SPINNER_DISABLED:
                    timer[0] = Constants.TIMER_DISABLED;
                    break;
                case TIMER_SPINNER_DAILY:
                    timer[0] = Constants.TIMER_DAILY;
                    break;
                case TIMER_SPINNER_SUNDAY:
                    timer[0] = Constants.TIMER_SUNDAY;
                    break;
                case TIMER_SPINNER_MONDAY:
                    timer[0] = Constants.TIMER_MONDAY;
                    break;
                case TIMER_SPINNER_TUESDAY:
                    timer[0] = Constants.TIMER_TUESDAY;
                    break;
                case TIMER_SPINNER_WEDNESDAY:
                    timer[0] = Constants.TIMER_WEDNESDAY;
                    break;
                case TIMER_SPINNER_THURSDAY:
                    timer[0] = Constants.TIMER_THURSDAY;
                    break;
                case TIMER_SPINNER_FRIDAY:
                    timer[0] = Constants.TIMER_FRIDAY;
                    break;
                case TIMER_SPINNER_SATURDAY:
                    timer[0] = Constants.TIMER_SATURDAY;
                    break;
            }
        } else {
            timer[0] = Constants.TIMER_DISABLED;
        }
        Bundle b = new Bundle();
        b.putIntArray("Timer", timer);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
    }
}

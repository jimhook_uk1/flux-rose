/**
 * *************************************************************************************************
 * Name: Control Light
 * Workfile: ControlLightActivity.java
 * Author: RDTek
 * Date: 12/12/15
 * <p/>
 * Copyright:  RD Technical Solutions
 * <p/>
 * Date     | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Updated Header.
 *          |       | Added onoff and brightness status indications. Modified devcomms interface.
 * 12/12/15 | RDTek | On/Off button changed to initialise to on/off state in device.
 * <p/>
 * Description:
 * *************************************************************************************************
 */
package rdteq.mooklight;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import rdteq.mooklight.HelperClasses.Constants;
import rdteq.mooklight.HelperClasses.Device;
import rdteq.mooklight.HelperClasses.DeviceComms;

import static rdteq.mooklight.R.drawable.bar_black2;
import static rdteq.mooklight.R.drawable.bar_blue2;
import static rdteq.mooklight.R.drawable.black_button_light2;
import static rdteq.mooklight.R.drawable.blue_button_light2;

public class ControlLightActivity extends ActionBarActivity {
    private static final String TAG = "CtrlLamp";

    static final int JUST_STARTED = 0;
    static final int WAITING_DIM = 1;
    static final int GETTING_ONOFF = 2;
    static final int WAITING_ONOFF = 3;
    static final int RUNNING = 4;
    static final int SHUT_DOWN = 5;

    static final int BRIGHTNESS_OFFSET = 10;

    static final int TIMER_TASK_DELAY = 100; //msecs
    static final int TIMER_TASK_PERIOD = 200; //msecs
    DeviceComms devComms;
    Resources res;

    private int brightness;
    private int commsState;
    private int onoff;
    private Context context;
    private Device connectedDevice;
    private Button onoffButton;
    private FrameLayout controlFLayout;
    private ProgressDialog progressDialog;
    private SeekBar brightnessSeekBar;
    private TextView brightnessStatus;
    private TextView onoffStatus;
    private Timer commsTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        context = this;
        connectedDevice = new Device();
        res = getResources();

        /* Get resources */
        setContentView(R.layout.activity_control_light);
        TextView title = (TextView) findViewById(R.id.controlDevName);
        brightnessSeekBar = (SeekBar) findViewById(R.id.controlBrightness);
        onoffStatus = (TextView) findViewById(R.id.statusOnOff);
        onoffStatus.setText(R.string.s_off);
        onoffButton = (Button) findViewById(R.id.controlOnOff);
        brightnessStatus = (TextView) findViewById(R.id.statusBrightness);
        brightnessStatus.setText(R.string.s_zeroPercent);
        Button doneButton = (Button) findViewById(R.id.doneControl);
        controlFLayout = (FrameLayout) findViewById(R.id.controlFrame);
        controlFLayout.setBackgroundResource(bar_black2);
        onoffButton.setBackgroundResource(black_button_light2);
        //brightnessSeekBar.setEnabled(false);
        //brightnessSeekBar.setVisibility(View.INVISIBLE);
        brightnessSeekBar.setOnSeekBarChangeListener(new OnBrightnessChangeListener(connectedDevice));
        brightnessSeekBar.setProgress(0);
        onoffButton.setOnClickListener(new OnOffClickListener(connectedDevice));
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitActivity(RESULT_OK);
            }
        });

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        connectedDevice.ipAdd = b.getString("IPAddress");
        connectedDevice.apType = b.getInt("APType");
        connectedDevice.SSID = b.getString("SSID");
        connectedDevice.BSSID = b.getString("BSSID");
        title.setText(b.getString("DevName"));

        devComms = new DeviceComms(context, devCommsMsgHdlr, connectedDevice);
    }

    @Override
    public void onResume() {
        super.onResume();
        commsState = JUST_STARTED;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Connecting..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        devComms.connect();
    }

    private class OnBrightnessChangeListener implements SeekBar.OnSeekBarChangeListener {
        private Device device;

        OnBrightnessChangeListener(Device dev) {
            device = dev;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (BuildConfig.DEBUG) Log.i(TAG, "Brightness " + progress + BRIGHTNESS_OFFSET);
            device.brightness = progress + BRIGHTNESS_OFFSET;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

    private class OnOffClickListener implements Button.OnClickListener {
        private Device device;

        OnOffClickListener(Device dev) {
            device = dev;
        }

        @Override
        public void onClick(View v) {
            if (device.onoff == 0) {
                onoffButton.setBackgroundResource(blue_button_light2);
                device.onoff = 1;
            } else {
                onoffButton.setBackgroundResource(black_button_light2);
                device.onoff = 0;
            }
            if (BuildConfig.DEBUG) Log.i(TAG, "OnOff " + device.onoff);
        }
    }

    private void lightStateChanged() {
        if (connectedDevice.lightOn == 1) {
            controlFLayout.setBackgroundResource(bar_blue2);
            onoffStatus.setText(R.string.s_on);
            //brightnessSeekBar.setEnabled(true);
            //brightnessSeekBar.setVisibility(View.VISIBLE);
        } else {
            controlFLayout.setBackgroundResource(bar_black2);
            onoffStatus.setText(R.string.s_off);
            //brightnessSeekBar.setEnabled(false);
            //brightnessSeekBar.setVisibility(View.INVISIBLE);
        }
    }

    private Handler devCommsMsgHdlr = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (commsState != SHUT_DOWN) {
                switch (msg.what) {
                    case DeviceComms.ABORTED:
                        exitActivity(RESULT_CANCELED);
                        break;

                    case DeviceComms.CONNECTED:
                        if (msg.arg1 == Device.LIGHT_CONTROLLER) {
                            //commsTimer = new Timer();
                            //commsTimer.schedule(new CommsTask(), 100, 100);
                            devComms.readDim();
                            break;
                        }
                        break;

                    case DeviceComms.XFER_ERROR:
                        String error;
                        switch (msg.arg1) {
                            case Constants.LOST_CONNECTION:
                                error = res.getString(R.string.s_lostConnection);
                                break;
                            case Constants.READ_IO_EXCEPTION:
                                error = res.getString(R.string.s_readIOException);
                                break;
                            case Constants.SOCKET_INIT_IO_EXCEPTION:
                                error = res.getString(R.string.s_socketInitIOException);
                                break;
                            case Constants.STREAM_INIT_IO_EXCEPTION:
                                error = res.getString(R.string.s_streamInitIOException);
                                break;
                            case Constants.UNKNOWN_HOST_EXCEPTION:
                                error = res.getString(R.string.s_unknownHostException);
                                break;
                            case Constants.WRITE_IO_EXCEPTION:
                                error = res.getString(R.string.s_writeIOException);
                                break;
                            default:
                                error = res.getString(R.string.s_unknownError);
                                break;
                        }
                        Toast.makeText(context.getApplicationContext(), error, Toast.LENGTH_LONG).show();
                        exitActivity(RESULT_CANCELED);
                        break;

                    case DeviceComms.TID_READ_DIM:
                        connectedDevice.brightness = msg.arg1;
                        brightness = connectedDevice.brightness;
                        brightnessStatus.setText(Integer.toString(brightness) + "%");
                        brightnessSeekBar.setProgress(brightness - BRIGHTNESS_OFFSET);
                        devComms.readOnOff();
                        commsState = GETTING_ONOFF;
                        break;

                    case DeviceComms.TID_READ_ONOFF:
                        connectedDevice.onoff = msg.arg1;
                        onoff = msg.arg1;
                        connectedDevice.onLine = true;
                        if (msg.arg1 == 0) {
                            onoffButton.setBackgroundResource(black_button_light2);
                        } else {
                            onoffButton.setBackgroundResource(blue_button_light2);
                        }
                        devComms.readLight();
                        commsState = RUNNING;
                        break;

                    case DeviceComms.TID_READ_LIGHT:
                        if (BuildConfig.DEBUG) Log.i(TAG, "LightState: " + msg.arg1);
                        if (connectedDevice.lightOn != msg.arg1) {
                            connectedDevice.lightOn = msg.arg1;
                            lightStateChanged();
                        }
                        brightness = connectedDevice.brightness;
                        onoff = connectedDevice.onoff;
                        devComms.writeDimOnOff(connectedDevice.brightness, connectedDevice.onoff);
                        break;

                    case DeviceComms.TID_WRITE_DIM:
                        brightness = msg.arg1;
                        devComms.readLight();
                        break;

                    case DeviceComms.TID_WRITE_ONOFF:
                        brightness = connectedDevice.brightness;
                        onoff = connectedDevice.onoff;
                        devComms.readLight();
                        break;

                    case DeviceComms.TID_WRITE_DIM_ONOFF:
                        brightnessStatus.setText(Integer.toString(((brightness - 10) * 5) / 4) + "%");
                        devComms.readLight();
                        break;
                }
            }
        }
    };

    class CommsTask extends TimerTask {
        int keepAliveCnt;

        @Override
        public void run() {
            switch (commsState) {
                case JUST_STARTED:
                    devComms.readDim();
                    commsState = WAITING_DIM;
                    break;
                case GETTING_ONOFF:
                    devComms.readOnOff();
                    commsState = WAITING_ONOFF;
                    break;
                case RUNNING:
                    if (brightness != connectedDevice.brightness) {
                        if (!devComms.isBusy()) {
                            devComms.writeDim(connectedDevice.brightness);
                        }
                        keepAliveCnt = 0;
                    } else if (onoff != connectedDevice.onoff) {
                        if (!devComms.isBusy()) {
                            devComms.writeOnOff(connectedDevice.onoff);
                        }
                        keepAliveCnt = 0;
                    } else {
                        if (!devComms.isBusy()) {
                            if (keepAliveCnt == 0) {
                                devComms.readLight();
                            }
                            keepAliveCnt++;
                            if (keepAliveCnt == 10) {
                                if (BuildConfig.DEBUG) Log.i(TAG, "Still running");
                                keepAliveCnt = 0;
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onPause");

        devComms.close();

        /* Stop Timer */
        commsState = SHUT_DOWN;
        if (commsTimer != null) {
            commsTimer.cancel();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (BuildConfig.DEBUG) Log.i(TAG, "onStop");
        super.onStop();
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                exitActivity(RESULT_OK);
//        }
//        return true;
//    }

//    @Override
//    public void onBackPressed() {
//        exitActivity(RESULT_OK);
//        super.onBackPressed();
//    }

    private void exitActivity(int result) {
        Bundle b = new Bundle();
        b.putInt("Brightness", connectedDevice.brightness);
        b.putInt("OnOff", connectedDevice.onoff);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(result, intent);
        finish();
    }
}
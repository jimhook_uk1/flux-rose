#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=MookLight.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/MookLight.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=mooklight.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/mooklight.x.tar
# FluxRose configuration
CND_ARTIFACT_DIR_FluxRose=dist/FluxRose/production
CND_ARTIFACT_NAME_FluxRose=MookLight.X.production.hex
CND_ARTIFACT_PATH_FluxRose=dist/FluxRose/production/MookLight.X.production.hex
CND_PACKAGE_DIR_FluxRose=${CND_DISTDIR}/FluxRose/package
CND_PACKAGE_NAME_FluxRose=mooklight.x.tar
CND_PACKAGE_PATH_FluxRose=${CND_DISTDIR}/FluxRose/package/mooklight.x.tar

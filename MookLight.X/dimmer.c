/***************************************************************************************************
 * Name: Dimmer
 * Workfile: dimer.c
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 01/08/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release.
 * 07/12/15 | RDTek | Updated Interrupt Priority to 7. Made accessor functions uninterruptable.
 *          |       | Linearised the dimming function!!
 *
 * Description:
 * Note: TMR2 resets when PR2 is reached!!!
 * Interrupt Priority 7
 * 
 **************************************************************************************************/

/**************************************************************************************** Include */
#include <xc.h>
#include "dimmer.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* NOTE: The settings assume a 16MHz peripheral clock Fp */
#define BRIGHT_SCALE        200u
#define DEFAULT_DIM_VAL     50u
#define DIM_SCALE           1u   /* /8 - 2MHz clock */
#define NUM_SYNCS_TO_START  10u
#define MAINS_PERIOD        20000u
#define MIN_BRIGHTNESS      10u
#define MAX_BRIGHTNESS      90u
#define SAMPLE_SW_TIME      500u
#define TRIAC_ON_TIME       100u
#define WINDOW_MAX          32768u
#define WINDOW_START        19000u
#define WINDOW_SIZE         2000u

#ifdef FLUXROSE
#define AC_SW_PIN   PORTAbits.RA0
#define OUT_PIN     LATBbits.LATB0
#define OUT_PIN_DDR TRISBbits.TRISB0
#else
#define AC_SW_PIN   PORTBbits.RB8
#define OUT_PIN     LATBbits.LATB4
#define OUT_PIN_DDR TRISBbits.TRISB4
#endif
/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Variables (static) */
static bool acSwitchOn;
static bool wifiSwitchOn;

static enum {
    WAITING_SAMPLE_SW,
    WAITING_SYNC_TRIGGER,
    WAITING_SYNC_WINDOW,
    WAITING_SYNC_CONFIRM,
    WAITING_TRIG_WINDOW,
    WAITING_TRIGGER,
    WAITING_ON,
    WAITING_OFF,
    WAITING_WINDOW
} dimState;
static uint16_t brightness;
static uint16_t dim;
static uint16_t longFlt;
static uint16_t shortFlt;
static uint8_t numSyncs;
static uint16_t time1;
static uint16_t timeMax;
static uint16_t timeMin;
static uint8_t acSwCnt;
#ifdef FLUXROSE
static bool supplyState;
#endif

/********************************************************************** Local Function Prototypes */

/*********************************************************************** Local Constants (static) */
static const uint16_t brightConvert[101] = {
    2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000,
    2000, 2000, 2000, 2000, 2000, 2000, 2046, 2175, 2304, 2434,
    2564, 2694, 2824, 2955, 3086, 3217, 3349, 3481, 3613, 3746,
    3879, 4013, 4147, 4282, 4417, 4553, 4689, 4826, 4963, 5101,
    5240, 5379, 5519, 5659, 5801, 5943, 6086, 6230, 6375, 6520,
    6667, 6814, 6963, 7112, 7263, 7415, 7568, 7722, 7878, 8035,
    8193, 8353, 8515, 8678, 8843, 9009, 9178, 9348, 9521, 9696,
    9873, 10052, 10234, 10419, 10607, 10798, 10992, 11190, 11391, 11597,
    11807, 12021, 12241, 12466, 12698, 12936, 13181, 13435, 13698, 13972,
    14257, 14557, 14872, 15208, 15567, 15957, 16387, 16873, 17449, 18000,
    18000
};

/*************************************************************************** Function Definitions */

#ifdef FLUXROSE
/********************************************************************************** _CNInterrupt ***
 * Name:        CN ISR
 * Parameters:  None
 * Returns:     None
 * Description: In conjunction with the _T2Interrupt handles the dimming function.
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _CNInterrupt(void) {

    IFS1bits.CNIF = 0;
    if (PORTAbits.RA1 == 0) {
#else
/******************************************************************************** _INT0Interrupt ***
 * Name:        INT0 ISR
 * Parameters:  None
 * Returns:     None
 * Description: In conjunction with the _T2Interrupt handles the dimming function.
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _INT0Interrupt(void) {

    IFS0bits.INT0IF = 0;
#endif
    
    time1 = TMR2;
    if (time1 < timeMin) {
        timeMin = time1;
    } else if (time1 > timeMax) {
        timeMax = time1;
    }
    TMR2 = 0;
    switch (dimState) {
        case WAITING_SYNC_TRIGGER:
            PR2 = WINDOW_START;
            numSyncs = 0;
            dimState = WAITING_SYNC_WINDOW;
            break;
        case WAITING_SYNC_WINDOW:
            /* Detects short signal */
            dimState = WAITING_SYNC_TRIGGER;
            break;
        case WAITING_SYNC_CONFIRM:
            if (numSyncs == NUM_SYNCS_TO_START) {
                timeMax = 0;
                timeMin = 65535;
                dimState = WAITING_TRIG_WINDOW;
            } else {
                numSyncs++;
                dimState = WAITING_SYNC_WINDOW;
            }
            PR2 = WINDOW_START;
            break;
        case WAITING_TRIG_WINDOW:
            shortFlt++;
            break;
        case WAITING_TRIGGER:
            PR2 = SAMPLE_SW_TIME;
            dimState = WAITING_SAMPLE_SW;
            break;
        case WAITING_ON:
        case WAITING_OFF:
        case WAITING_WINDOW:
            shortFlt++;
            break;
        default:
            dimState = WAITING_SYNC_TRIGGER;
            break;
    }

#ifdef FLUXROSE
    }
#endif
}

/********************************************************************************** _T2Interrupt ***
 * Name:        T2 ISR
 * Parameters:  None
 * Returns:     None
 * Description: In conjunction with the _INT0Interrupt handles the dimming function.
 *              Note that TMR0 is reset to 0 when an interrupt occurs.
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _T2Interrupt(void) {
    IFS0bits.T2IF = 0;

    switch (dimState) {
        case WAITING_SYNC_TRIGGER:
            break;
        case WAITING_SYNC_WINDOW:
            PR2 = WINDOW_SIZE;
            dimState = WAITING_SYNC_CONFIRM;
            break;
        case WAITING_SYNC_CONFIRM:
            /* Detects long signal */
            longFlt++;
            dimState = WAITING_SYNC_TRIGGER;
            break;
        case WAITING_TRIG_WINDOW:
            PR2 = WINDOW_SIZE;
            dimState = WAITING_TRIGGER;
            break;
        case WAITING_TRIGGER:
            /* Detects long signal */
            longFlt++;
            dimState = WAITING_SYNC_TRIGGER;
            break;
        case WAITING_SAMPLE_SW:
            if (AC_SW_PIN == 0) {
                acSwitchOn = true;
            } else {
                acSwitchOn = false;
            }
            if (acSwitchOn != wifiSwitchOn) {
                dim = MAINS_PERIOD - brightness - SAMPLE_SW_TIME;
                PR2 = dim;
                dimState = WAITING_ON;
            } else {
                PR2 = WINDOW_START - SAMPLE_SW_TIME;
                dimState = WAITING_TRIG_WINDOW;
            }
            break;
        case WAITING_ON:
            OUT_PIN = 1;
            PR2 = TRIAC_ON_TIME;
            dimState = WAITING_OFF;
            break;
        case WAITING_OFF:
            OUT_PIN = 0;
            PR2 = WINDOW_START - dim - TRIAC_ON_TIME;
            dimState = WAITING_WINDOW;
            break;
        case WAITING_WINDOW:
            PR2 = WINDOW_SIZE;
            dimState = WAITING_TRIGGER;
            break;
        default:
            dimState = WAITING_SYNC_TRIGGER;
            break;
    }
}

/************************************************************************** dimmer_getLightState ***
 ** Name:        Get Light State
 ** Parameters:  None
 ** Returns:     true if light is on
 ** Description: Gets current light State
 **************************************************************************************************/
bool dimmer_getLightState(void) {
    bool rv;
    
    rv = false;
#ifdef FLUXROSE
    IEC1bits.CNIE = 0;
    if (acSwitchOn != wifiSwitchOn) {
        rv = true;
    }
    IEC1bits.CNIE = 1;
#else
    IEC0bits.INT0IE = 0;
    if (acSwitchOn != wifiSwitchOn) {
        rv = true;
    }
    IEC0bits.INT0IE = 1;
#endif
    
    return rv;
}

/*********************************************************************************** dimmer_init ***
 * Name:        Dimmer Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the dimmer functionality.
 **************************************************************************************************/
void dimmer_init(void) {

    /* Initialise triac driver */
    OUT_PIN = 0;
    OUT_PIN_DDR = 0;
    
    /* Initialise state machine and brightness */
    dimState = WAITING_SYNC_TRIGGER;
    brightness = DEFAULT_DIM_VAL * BRIGHT_SCALE;
    timeMax = 0;
    timeMin = 65535;
    
    /* Initialise Timer 2 for dimming function */
    PR2 = WINDOW_MAX;
    T2CONbits.TCKPS = DIM_SCALE;
    T2CONbits.TON = 1;
    IPC1bits.T2IP = 7;
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;

    /* Initialise Zero crossing Monitors */
#ifdef FLUXROSE
    IPC4bits.CNIP = 7;
    CNENAbits.CNIEA1 = 1;
    IFS1bits.CNIF = 0;
    IEC1bits.CNIE = 1;
 #else
    INTCON2bits.INT0EP = 1; /* Negative edge */
    IPC0bits.INT0IP = 7;
    IFS0bits.INT0IF = 0;
    IEC0bits.INT0IE = 1;
    INTCON2bits.INT1EP = 1;
    //IPC5bits.INT1IP = 5;
    //IFS1bits.INT1IF = 0;
    //IEC1bits.INT1IE = 1;
#endif
}

/************************************************************************** dimmer_setBrightness ***
 * Name:        Set brightness
 * Parameters:  value - 0 - 100%
 * Returns:     None
 * Description: Sets the brightness of the dimmer.
 **************************************************************************************************/
void dimmer_setBrightness(uint8_t value) {
    uint16_t uv;
#if(0)    
    if (value < MIN_BRIGHTNESS) {
        uv = MIN_BRIGHTNESS * BRIGHT_SCALE;
    } else if (value > MAX_BRIGHTNESS) {
        uv = MAX_BRIGHTNESS * BRIGHT_SCALE;
    } else {
        uv = value * BRIGHT_SCALE;
    }
#else
    if (value > 100) {
        value = 100;
    }
    uv = brightConvert[value];
#endif
#ifdef FLUXROSE
    IEC1bits.CNIE = 0;
    brightness = uv;
    IEC1bits.CNIE = 1;
#else    
    IEC0bits.INT0IE = 0;
    brightness = uv;
    IEC0bits.INT0IE = 1;
#endif
}

/*************************************************************************** dimmer_setWifiSwitch ***
 * Name:        Set Wifi Switch
 * Parameters:  value - true/false
 * Returns:     None
 * Description: Sets the wifi switch status.
 **************************************************************************************************/
void dimmer_setWifiSwitch(bool value) {
#ifdef FLUXROSE
    IEC1bits.CNIE = 0;
    wifiSwitchOn = value;
    IEC1bits.CNIE = 1;
#else    
    IEC0bits.INT0IE = 0;
    wifiSwitchOn = value;
    IEC0bits.INT0IE = 1;
#endif
}

/*
 * End of Workfile: dimmer.c
 */


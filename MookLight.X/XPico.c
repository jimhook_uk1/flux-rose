/***************************************************************************************************
 * Name: XPico Data Link Layer
 * Workfile: XPico.c
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 30/07/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release
 * 17/10/15 | RDTek | Tidied up. Removed string library calls.
 * 07/12/15 | RDTek | Numerous modifications to clarify functionality and to include timeouts
 *
 * Description:
 * This module handles the interface with the XPico Wifi Module.
 * Modbus Header format:
 *                  Transaction Identifier      2bytes
 *                  Protocol Identifier (=0)    2bytes
 *                  Length (Num following bytes)2bytes
 *                  Unit Identifer              1byte 
 **************************************************************************************************/

/**************************************************************************************** Include */
#include <xc.h>
#include "serial.h"
#include "modbus.h"
#include "XPico.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define DEFAULT_PIN LATBbits.LATB13
#define RST_PIN     LATBbits.LATB14
#define WAKE_PIN    LATBbits.LATB12

#define MB_HDR_SIZE 7u
#define PICO_RESET_TIME 50u
#define RX_BUF_SIZE 250u
#define TX_BUF_SIZE 250u

#define ONE_TRANSFER

#define RESP_NO_RESP 0
#define RESP_OK      1
#define RESP_ERROR   2

#define TIMEOUT 50u
/********************************************************************************* Local TypeDefs */
enum responses {
    OK,
    VALUE_OK,
    STRING_LF,
    LFEED
};

/*********************************************************************** Local Variables (static) */
static bool timedOut;
static uint16_t timer;
static uint8_t binpRxBuf[RX_BUF_SIZE / 2];
static uint8_t binpTxBuf[TX_BUF_SIZE / 2];
static uint8_t channel;
static uint8_t rxIdx;
static uint8_t pRxBuf[RX_BUF_SIZE];
static uint8_t state;
static uint8_t pTxBuf[TX_BUF_SIZE];


/********************************************************************** Local Function Prototypes */
static bool compareStr(const uint8_t str1[], uint8_t str2[]);
static uint8_t convertDecimal(uint8_t dest[], uint8_t number);
static uint8_t getCmdResponse(void);
static uint8_t getHDResponse(void);
static void getHexData(uint8_t dest[], uint8_t src[], uint16_t num);
static uint8_t getHexNibble(uint8_t hexData);
static void putHexData(uint8_t dest[], uint8_t src[], uint16_t num);
static void swapChannel(void);
static void txChMsg(const uint8_t ch, const uint8_t msg[]);

/*********************************************************************** Local Constants (static) */

/*************************************************************************** Function Definitions */

/************************************************************************************ XPico_init ***
 ** Name:        Initialisation
 ** Parameters:  None
 ** Returns:     None
 ** Description: Initialises UART1 and various GPIO to interface to XPico Device
 **************************************************************************************************/
void XPico_init(void) {
    DEFAULT_PIN = 1;
    RST_PIN = 1;
    WAKE_PIN = 1;
    TRISB &= 0x8FFFu;
    timer = PICO_RESET_TIME;
    state = 0;
    serial_init();
}

/************************************************************************************ XPico_main ***
 ** Name:        Main task
 ** Parameters:  None
 ** Returns:     None
 ** Description: Called every 10msecs
 **************************************************************************************************/
void XPico_main(void) {
    uint8_t i;

    switch (state) {
        case 0:
#ifdef TSTBUF
            serial_logOn();
#endif
            if (timer == 0) {
                /* Get Channel Status*/
                txChMsg('1', (uint8_t *)"");
                state = 1;
            } else if (serial_getRxCnt() > 0) {
                (void)serial_getRxByte();
            }
            break;

        case 1:
            if (getCmdResponse() == RESP_OK) {
                if (pRxBuf[0] == '\n') {
                    /* Channel instance was active */
                    state = 2;
                } else if (pRxBuf[0] == 'D') {
                    txChMsg('1', (uint8_t *)"");
                    state = 4;
                } else {
                    txChMsg('1', (uint8_t *)"k");
                    state = 3;
                }
            }
            break;

        case 2:
            if (getCmdResponse() == RESP_OK) {
                txChMsg('1', (uint8_t *)"k");
                state = 3;
            }
            break;
            
        case 3:
            if (getCmdResponse() == RESP_OK) {
                /* Get Channel Status*/
                txChMsg('1', (uint8_t *)"");
                state = 4;
            }
            break;
            
        case 4:
            if (getCmdResponse() == RESP_OK) {
                switch (pRxBuf[0]) {
                    case 'K':
#ifdef TSTBUF
                        serial_logOn();
#endif
                        /* Get Header */
                        txChMsg('1', (uint8_t *) "rx7");
                        state = 5;
                        break;
                    case 'D':
                        txChMsg('1', (uint8_t *)"a502TCP");
                        state = 3;
                        break;
                    case 'F':
                        txChMsg('1', (uint8_t *)"e");
                        state = 3;
                        break;
#ifdef TSTBUF
                    case 'W':
                        serial_logOff();
                        txChMsg('1', (uint8_t *)"");
                        break;
#endif
                    default:
                        txChMsg('1', (uint8_t *)"");
                        break;
                }
            }
            break;
            
        case 5:
            if (getCmdResponse() == RESP_OK) {
                if (pRxBuf[0] == 'E') {
                    /* E End of File? */
                    txChMsg('1', (uint8_t *)"e");
                    state = 3;
                } else {
                    /* Dummy for K */
                    timer = TIMEOUT;
                    state = 6;
                }
            }
            break;
            
        case 6:
            if (getHDResponse() == RESP_OK) {
                if (pRxBuf[0] == '\n') {
                    if (!timedOut) {
                        /* Closed */
                        txChMsg('1', (uint8_t *)"e");
                    } else {
                        timedOut = false;
                        txChMsg('1', (uint8_t *)"k");
                    }
                    state = 3;
                } else {
                    /* Get Payload */
                    getHexData(binpRxBuf, pRxBuf, MB_HDR_SIZE);
                    pTxBuf[0] = 'r';
                    pTxBuf[1] = 'x';
                    binpRxBuf[5]--;
                    i = convertDecimal(&pTxBuf[2], binpRxBuf[5]);
                    pTxBuf[i + 2] = 0u;
                    txChMsg('1', pTxBuf);
                    state = 8;
                }
            } else if (timer == 0) {
                if (!timedOut) {
                    /* Timeout */
                    serial_txMsg((uint8_t *)"\n");
                    timedOut = true;
                }
            }
            break;
            
        case 8:
            if (getCmdResponse() == RESP_OK) {
                /* Dummy for K */
                state = 9;
            }
            break;

        case 9:
            if (getHDResponse() == RESP_OK) {
                /* Process ModBus Message */
                getHexData(&binpRxBuf[MB_HDR_SIZE], pRxBuf, binpRxBuf[5]);
                binpRxBuf[5]++;
                modbus_processMsg(binpTxBuf, binpRxBuf);
                putHexData(pTxBuf, binpTxBuf, binpTxBuf[5] + 6);
                txChMsg('1', (uint8_t *)"sx");
                state = 10;
            }
            break;
            
        case 10:
            if (getCmdResponse() == RESP_OK) {
                serial_txMsg(pTxBuf);
                state = 11;
            }
            break;
            
        case 11:
            if (getCmdResponse() == RESP_OK) {
                txChMsg('1', (uint8_t *)"p");
                state = 3;
            }
            break;
            
        default:
            state = 0;
            break;
    }
}

/*********************************************************************************** XPico_timer ***
 ** Name:        Timer task
 ** Parameters:  None
 ** Returns:     None
 ** Description: Called every 100msecs
 **************************************************************************************************/
void XPico_timer(void) {
    if (timer != 0) {
        timer--;
    }
}

/************************************************************************************ compareStr ***
 * Name:        Compare String
 * Parameters:  str1
 *              str2
 * Returns:     true if strings are the same
 * Description: Compares str1 and str2
 **************************************************************************************************/
static bool compareStr(const uint8_t str1[], uint8_t str2[]) {
    bool rv;
    int i;

    while ((str1[i] != 0) && (str1[i] == str2[i])) {
        i++;
    }
    if ((str1[i] == 0) && str2[i] == 0) {
        rv = true;
    } else {
        rv = false;
    }
    return rv;
}

/******************************************************************************** convertDecimal ***
 * Name:        Convert to ASCII Decimal
 * Parameters:  dest
 *              number
 * Returns:     Number of characters added to stream
 * Description: Converts number to ASCII decimal representation and puts in buffer starting at dest
 **************************************************************************************************/
static uint8_t convertDecimal(uint8_t dest[], uint8_t number) {
    uint8_t rval;

    if (number > 99) {
        dest[0] = (number / 100) + '0';
        dest[1] = ((number % 100) / 10) + '0';
        dest[2] = (number % 10) + '0';
        rval = 3;
    } else if (number > 9) {
        dest[0] = (number / 10) + '0';
        dest[1] = (number % 10) + '0';
        rval = 2;
    } else {
        dest[0] = number + '0';
        rval = 1;
    }
    return rval;
}

/************************************************************************************* getHexData ***
 * Name:        Get Hex Data
 * Parameters:  dest - Destination
 *              src  - Source
 *              num  - Number of bytes
 * Returns:     None
 * Description: Get num bytes from the serial interface.
 **************************************************************************************************/
static void getHexData(uint8_t dest[], uint8_t src[], uint16_t num) {
    uint16_t i, j;

    j = 0u;
    for (i = 0u; i < num; i++) {
        dest[i] = getHexNibble(src[j]) << 4;
        j++;
        dest[i] += getHexNibble(src[j]);
        j++;
    }
}

/********************************************************************************** getHexNibble ***
 * Name:        Get Hex Nibble
 * Parameters:  hexData - encoded ASCII Hex Nibble
 * Returns:     nibble  - decimal nubble value
 * Description: Decodes an ASCII Hex Nibble
 **************************************************************************************************/
static uint8_t getHexNibble(uint8_t hexData) {
    uint8_t rval;

    if (hexData >= 'A') {
        rval = hexData - 'A' + 10u;
    } else {
        rval = hexData - '0';
    }
    return rval;
}

/************************************************************************************* putHexData ***
 * Name:        Put Hex Data
 * Parameters:  dest - Destination
 *              src - Source
 *              num - Number of bytes
 * Returns:     None
 * Description: Get num bytes from the serial interface.
 *              The routine assumes that the first byte is the positive response is 'K'
 **************************************************************************************************/
static void putHexData(uint8_t dest[], uint8_t src[], uint16_t num) {
    uint16_t i, j;
    uint8_t hexval;

    j = 0u;
    for (i = 0u; i < num; i++) {
        hexval = (src[i] >> 4);
        if (hexval > 9u) {
            dest[j] = hexval + 'A' - 10u;
        } else {
            dest[j] = hexval + '0';
        }
        j++;
        hexval = src[i] & 0x0F;
        if (hexval > 9u) {
            dest[j] = hexval + 'A' - 10u;
        } else {
            dest[j] = hexval + '0';
        }
        j++;
    }
    dest[j] = '\n';
    j++;
    dest[j] = 0u;
}

/************************************************************************************ swapChannel ***
 * Name:        Swap Channel
 * Parameters:  None
 * Returns:     None
 * Description: Changes current channel to other channel
 **************************************************************************************************/
static void swapChannel(void) {
    if (channel == '1') {
        channel = '2';
    } else {
        channel = '1';
    }
}

/*************************************************************************************** txChMsg ***
 * Name:        Transmit Wifi Channel Message
 * Parameters:  ch - channel
 *              msg - message
 * Returns:     True when a valid response is received
 * Description: Handles normal response messages from the Xpico
 **************************************************************************************************/
static void txChMsg(const uint8_t ch, const uint8_t msg[]) {
    uint8_t buf[255];
    uint8_t i, j;

    buf[0] = ch;
    i = 0;
    j = 1;
    while (msg[i] != 0) {
        buf[j] = msg[i];
        i++;
        j++;
    }
    buf[j++] = '\n';
    buf[j] = 0u;
    serial_txMsg(buf);
}

/******************************************************************************** getCmdResponse ***
 * Name:        Get Command Response
 * Parameters:  None
 * Returns:     True when a valid response is received
 * Description: Handles normal response messages from the Xpico
 **************************************************************************************************/
static uint8_t getCmdResponse(void) {
    uint8_t rv;
    uint8_t rxdata;

    rv = RESP_NO_RESP;
    while ((serial_getRxCnt() > 0u) &&
            (rv == RESP_NO_RESP)) {
        rxdata = serial_getRxByte();
        if (rxIdx == 0) {
            switch (rxdata) {
                case 'D':
                case 'F':
                case 'K':
                case 'R':
                case 'T':
                case 'W':
                case '\n':
                    pRxBuf[0] = rxdata;
                    rv = RESP_OK;
                    break;
                default:
                    pRxBuf[0] = rxdata;
                    rxIdx = 1;
                    break;
            }
        } else {
            if (rxdata == '\n') {
                pRxBuf[rxIdx] = 0u;
                rxIdx = 0;
                rv = RESP_OK;
            } else if (rxdata == 'K') {
                pRxBuf[rxIdx] = 'K';
                rxIdx = 0;
                rv = RESP_OK;
            } else {
                pRxBuf[rxIdx] = rxdata;
                rxIdx++;
            }
        }
    }
    return rv;
}

/********************************************************************************** getHDResponse ***
 * Name:        Get Hex Data Response
 * Parameters:  None
 * Returns:     True when a valid response is received
 * Description: Handles data response from Xpico
 **************************************************************************************************/
static uint8_t getHDResponse(void) {
    uint8_t rv;
    uint8_t rxdata;

    rv = RESP_NO_RESP;
    while ((serial_getRxCnt() > 0u) &&
            (rv == RESP_NO_RESP)) {
        rxdata = serial_getRxByte();
        if (rxdata == '\n') {
            if (rxIdx == 0) {
                pRxBuf[0] = rxdata;
            } else {
                pRxBuf[rxIdx] = 0u;
            }
            rxIdx = 0;
            rv = RESP_OK;
        } else {
            pRxBuf[rxIdx] = rxdata;
            rxIdx++;
        }
    }
    return rv;
}

/*
 * End of Workfile: XPico.c
 */



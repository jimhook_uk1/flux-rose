/***************************************************************************************************
 * Name: Timers Header
 * Workfile: timers.h
 * Author: RDTek
 * Date: 08/11/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 08/11/15 | RDTek | Moved timer functionality from main.
 *
 * Description:
 **************************************************************************************************/

#ifndef TIMERS_H
#define TIMERS_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define CURRENT_TIME 0
#define START_ALARM_TIMES 2
#define TIMERS_NUM_TIMERS  30

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************* Global Function Prototypes */

/******************************************************************************** app_timerEvent ***
 * Name:        Timer Event Callback
 * Parameters:  lightEvent (true = On, false = Off)
 * Returns:     None
 * Description: 
 **************************************************************************************************/
extern void app_timerEvent(bool lightEvent);

/******************************************************************************* timers_getTimer ***
 * Name:        Get Timer
 * Parameters:  idx
 * Returns:     None
 * Description: Get current Timer value
 **************************************************************************************************/
extern int16_t timers_getTimer(uint16_t idx);

    /*********************************************************************************** timers_tick ***
 * Name:        Timers Tick
 * Parameters:  None
 * Returns:     None
 * Description: Handles Timer update every minute
 **************************************************************************************************/
extern void timers_tick(void);

/********************************************************************************* timers_update ***
 * Name:        Update Timers
 * Parameters:  appTimers
 *              idx
 * Returns:     None
 * Description: Updates Timers
 **************************************************************************************************/
extern void timers_update(int16_t appTimer, uint16_t idx);

#endif /* TIMERS_H */
/*
 * End of Workfile: timers.h
 */


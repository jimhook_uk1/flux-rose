/***************************************************************************************************
 * Name:    I2C Driver Header
 * File:    i2c.h
 * Author:  RDTek
 * Date:    11/02/17
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 11/02/17 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef I2C_H
#define I2C_H
/**************************************************************************************** Include */
#include <xc.h>
//#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define I2C_BRG 250
#define I2C_STATUS_ACK              0
#define I2C_STATUS_IDLE             1
#define I2C_STATUS_NAK              2
#define I2C_STATUS_READ_DONE        3
#define I2C_STATUS_TFER_IN_PROGRESS 4

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************************* i2c_u8GetData ***
 * Name:        Get Data
 * Parameters:  None
 * Returns:     u8RxData
 * Description: Retrieves data from the buffer
 **************************************************************************************************/
extern uint8_t i2c_u8GetData(void);

/******************************************************************************** i2c_u8GetStatus***
 * Name:        Get Status
 * Parameters:  None
 * Returns:     u8I2CStatus
 * Description: Reads I2C drive status
 **************************************************************************************************/
extern uint8_t i2c_u8GetStatus(void);

/************************************************************************************* i2c_vInit ***
 * Name:        Initialise I2c Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
extern void i2c_vInit(void);

/********************************************************************************** i2c_vReadAck ***
 * Name:        Read Data transmit Ack
 * Parameters:  None
 * Returns:     None
 * Description: Starts an I2C read transfer and responds with Ack
 **************************************************************************************************/
extern void i2c_vReadAck(void);

/********************************************************************************** i2c_vReadNak ***
 * Name:        Read Data transmit Nak
 * Parameters:  None
 * Returns:     None
 * Description: Starts an I2C read transfer and responds with Nak
 **************************************************************************************************/
extern void i2c_vReadNak(void);

/*********************************************************************************** i2c_vTxByte ***
 * Name:        Transmit Byte
 * Parameters:  byte
 * Returns:     None
 * Description: Transmits 'byte' and waits for ack or nak
 *************************************************************************************************/
extern void i2c_vTxByte(uint8_t byte);

/******************************************************************************** i2c_vTxReStart ***
 * Name:        Transmit ReStart
 * Parameters:  startByte
 * Returns:     None
 * Description: Transmits an I2C ReStart followed by 'startByte' and waits for ack or nak
 *************************************************************************************************/
extern void i2c_vTxReStart(uint8_t startByte);

/********************************************************************************** i2c_vTxStart ***
 * Name:        Transmit Start
 * Parameters:  startByte
 * Returns:     None
 * Description: Transmits an I2C Start followed by 'startByte' and waits for ack or nak
 *************************************************************************************************/
extern void i2c_vTxStart(uint8_t startByte);

/********************************************************************************** i2c_vTxStop ***
 * Name:        Transmit Stop
 * Parameters:  None
 * Returns:     None
 * Description: Transmits an I2C Stop
 *************************************************************************************************/
extern void i2c_vTxStop(void);

#endif /* I2C_H */
/*
 * End of Workfile: main.c
 */

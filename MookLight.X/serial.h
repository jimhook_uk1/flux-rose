/***************************************************************************************************
 * Name: Serial Interface Header
 * Workfile: serial.h
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 30/07/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release
 * 17/10/15 | RDTek | Tidied up
 * 07/12/15 | RDTek | Added functions to enable appliction to turn on/off logging functionality.
 *
 * Description:
 **************************************************************************************************/

#ifndef SERIAL_H
#define SERIAL_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define TSTBUF

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************* Global Function Prototypes */

/******************************************************************************* serial_getRxByte ***
 * Name:        Get Received Byte
 * Parameters:  None
 * Returns:     rxByte
 * Description: Retrieves a data byte from the receive buffer
 **************************************************************************************************/
extern uint8_t serial_getRxByte(void);

/******************************************************************************* serial_getRxCnt ***
 * Name:        Get Received Count
 * Parameters:  None
 * Returns:     rxCnt
 * Description: Retrieves number of bytes in data buffer
 **************************************************************************************************/
extern uint16_t serial_getRxCnt(void);

/*********************************************************************************** serial_init ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises specified SCI to operate as a generic test port
 **************************************************************************************************/
extern void serial_init(void);

#ifdef TSTBUF
/********************************************************************************** serial_logOff ***
 * Name:        Switch Logging Off
 * Parameters:  None
 * Returns:     None
 * Description: Switches off the logger
 **************************************************************************************************/
extern void serial_logOff(void);

/*********************************************************************************** serial_logOn ***
 * Name:        Switch Logging On
 * Parameters:  None
 * Returns:     None
 * Description: Switches n the logger
 **************************************************************************************************/
extern void serial_logOn(void);
#endif

/********************************************************************************** serial_txMsg ***
 * Name:        Send Message
 * Parameters:  msg - message to transmit
 * Returns:     None
 * Description: Transmits the contents of txBuf upto and including a <LF> character
 **************************************************************************************************/
extern void serial_txMsg(const uint8_t msg[]);

#endif /* SERIAL_H */
/*
 * End of Workfile: serial.h
 */


/***************************************************************************************************
 * Name: Modbus Header
 * Workfile: modbus.h
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 30/07/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release.
 * 10/10/15 | RDTek | Added time variables. Tidied up.
 * 07/12/15 | RDTek | Removed modbus register definitions. Added callbacks to get/set registers
 *          |       | values from App. Added init function to pass number of registers.
 *
 * Description:
 **************************************************************************************************/

#ifndef MODBUS_H
#define MODBUS_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "timers.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************* Global Function Prototypes */

/************************************************************************************ app_getReg ***
 * Name:        Get register value
 * Parameters:  RegId
 * Returns:     register value
 * Description: Gets the value of the specified register
 **************************************************************************************************/
extern int16_t app_getReg(uint16_t reg);

/************************************************************************************ app_putReg ***
 * Name:        Set register value
 * Parameters:  RegId
 *              value
 * Returns:     None
 * Description: Sets the value of the specified register
 **************************************************************************************************/
extern void app_putReg(uint16_t reg, int16_t value);

/*********************************************************************************** modbus_init ***
 * Name:        Initialise
 * Parameters:  numReg  - Number of registers
 * Returns:     None
 * Description: Initilises the modbus processor
 **************************************************************************************************/
extern void modbus_init(uint16_t numReg);

    /***************************************************************************** modbus_processMsg ***
 * Name:        Process Message
 * Parameters:  txBuf   - Transmit Buffer
 *              rxBuf   - Received message
 * Returns:     None
 * Description: Gets the status of the message transmitter
 **************************************************************************************************/
extern void modbus_processMsg(uint8_t txBuf[], uint8_t rxBuf[]);

#endif /* MODBUS_H */

/*
 * End of Workfile: modbus.h
 */

/***************************************************************************************************
 * Name: Dimmer Header
 * Workfile: dimmer.h
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 01/08/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release
 * 07/12/15 | RDTek | Removed app_10msTask. Module no longer provides timer.
 *          |       | Added dimmer_getLightStte to allow access to light state from App.
 *
 * Description:
 **************************************************************************************************/

#ifndef DIMMER_H
#define DIMMER_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************* Global Function Prototypes */

/************************************************************************** dimmer_getLightState ***
 ** Name:        Get Light State
 ** Parameters:  None
 ** Returns:     true if light is on
 ** Description: Gets current light State
 **************************************************************************************************/
extern bool dimmer_getLightState(void);

/*********************************************************************************** dimmer_init ***
 * Name:        Dimmer Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the dimmer functionality.
 **************************************************************************************************/
extern void dimmer_init(void);

/************************************************************************** dimmer_setBrightness ***
 * Name:        Set brightness
 * Parameters:  value - 0 - 100%
 * Returns:     None
 * Description: Sets the brightness of the dimmer.
 **************************************************************************************************/
extern void dimmer_setBrightness(uint8_t value);

/*************************************************************************** dimmer_setWifiSwitch ***
 * Name:        Set Wifi Switch
 * Parameters:  value - true/false
 * Returns:     None
 * Description: Sets the wifi switch status.
 **************************************************************************************************/
extern void dimmer_setWifiSwitch(bool value);

#endif /* DIMMER_H */
/*
 * End of Workfile: dimmer.h
 */


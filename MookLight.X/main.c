/***************************************************************************************************
 * Name: main
 * Workfile: main.c
 * Author: RDTek
 * Date: 15/10/17
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 01/08/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release.
 * 10/10/15 | RDTek | Added timer functionality. Tidied up.
 * 07/12/15 | RDTek | Moved timer functionality to timer.c and timer.h.
 *          |       | Modified interfaces to dimmer, modbus and Xpico functions.
 *          |       | Added RTC timer functionality to seperate timer from mains frequency.
 * 15/10/17 | RDTek | Changes for FluxRose Hardware
 * 
 * Description:
 * Fosc=64MHz,Fcy=32MHz,Fp=32MHz
 * INT0 detects AC signal on falling edge
 * T2 controls switching of Triac
 **************************************************************************************************/


/**************************************************************************************** Include */
#include <xc.h>
//#include <string.h>
#include "dimmer.h"
#include "i2c.h"
#include "modbus.h"
#include "timers.h"
#include "XPico.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define FOSC 64u
#define HDC1000_RD 0x85
#define HDC1000_WR 0x84
#define LIGHT_EVENT_IDLE 0
#define LIGHT_EVENT_ON 1
#define LIGHT_EVENT_OFF 2

/* NOTE: The settings assume a 16MHz peripheral clock Fp */
#define RTC_SCALE  3u
#define RTC_PERIOD 624u

#define MODBUS_DEV_NAME        0
#define MODBUS_DEV_TYPE        10
#define MODBUS_SSID            11
#define MODBUS_PASSWORD        27
#define MODBUS_UPDATE          43
#define MODBUS_BRIGHTNESS      44
#define MODBUS_LIGHTSWITCH     45
#define MODBUS_LIGHTSTATE      46
#define MODBUS_CURRENT_TIME    48
#define MODBUS_ALARM_TIME      50
#define MODBUS_NUM_TIMERS      (TIMERS_NUM_TIMERS - 2)
#define NUM_REGS (MODBUS_ALARM_TIME + MODBUS_NUM_TIMERS)
/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Variables (static) */
static bool bHdc1000Ev;
static bool hundredMsecEv;
static bool minuteEv;
static uint16_t u16Humidity;
static uint16_t u16Light;
static uint16_t u16Temperature;
static uint32_t onTime;
static uint8_t u8Hdc1000State;
static uint8_t lightCtrl;
static uint8_t minuteCnt;
static uint8_t tick;
int16_t modbus_reg[NUM_REGS];

/********************************************************************** Local Function Prototypes */

/*********************************************************************** Local Constants (static) */
/* Configuration Settings */
#pragma config ICS = PGD1
#pragma config JTAGEN = OFF
#pragma config ALTI2C1 = ON
#pragma config ALTI2C2 = OFF
#pragma config WDTWIN = WIN75
#pragma config WDTPOST = PS32768
#pragma config WDTPRE = PR128
#pragma config PLLKEN = ON
#pragma config WINDIS = OFF
#pragma config FWDTEN = OFF
#pragma config POSCMD = XT
#pragma config OSCIOFNC = ON
#pragma config IOL1WAY = ON
#pragma config FCKSM = CSECMD
#pragma config FNOSC = FRC
#pragma config IESO = OFF
#pragma config GWRP = OFF
#pragma config GCP = OFF

#ifdef FLUXROSE
static const uint8_t defaultName[] = "FluxRose";
#else
static const uint8_t defaultName[] = "MookLight";
#endif
/*************************************************************************** Function Definitions */
static void vHdc1000Main(void);
static void rtcInit(void);

/********************************************************************************** _T1Interrupt ***
 * Name:        Timer1 ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: Occurs every 10msec
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _T1Interrupt(void) {
    IFS0bits.T1IF = 0;
    onTime++;
    tick++;
    if (tick == 100u) {
        tick = 0u;
        minuteCnt++;
        if (minuteCnt == 60) {
            minuteCnt = 0;
            minuteEv = true;
        }
    }
    if ((tick % 10) == 0) {
        hundredMsecEv = true;
        bHdc1000Ev = true;
    }
}

/************************************************************************************ app_getReg ***
 * Name:        Get register value
 * Parameters:  RegId
 * Returns:     register value
 * Description: Gets the value of the specified register
 **************************************************************************************************/
int16_t app_getReg(uint16_t reg) {
    int16_t rv;
    
    switch(reg) {
        case MODBUS_LIGHTSTATE:
            if (dimmer_getLightState()) {
                rv = 1;
            } else {
                rv = 0;
            }
            break;
        default:
            if ((reg >= MODBUS_CURRENT_TIME) &&
                    (reg < (MODBUS_CURRENT_TIME + TIMERS_NUM_TIMERS))) {
                rv = timers_getTimer(reg - MODBUS_CURRENT_TIME);
            } else {
                rv = modbus_reg[reg];
            }
            break;
        
    }
    
    return rv;
}

/************************************************************************************ app_putReg ***
 * Name:        Set register value
 * Parameters:  RegId
 *              value
 * Returns:     None
 * Description: Sets the value of the specified register
 **************************************************************************************************/
void app_putReg(uint16_t reg, int16_t value) {
    switch (reg) {
        case MODBUS_BRIGHTNESS:
            modbus_reg[MODBUS_BRIGHTNESS] = value;
            dimmer_setBrightness(value);
            break;
        case MODBUS_LIGHTSWITCH:
            modbus_reg[MODBUS_LIGHTSWITCH] = value;
            if (value == 1u) {
                dimmer_setWifiSwitch(true);
            } else {
                dimmer_setWifiSwitch(false);
            }
            break;
        default:
            if ((reg >= MODBUS_CURRENT_TIME) &&
                    (reg < (MODBUS_CURRENT_TIME + TIMERS_NUM_TIMERS))) {
                timers_update(value, reg - MODBUS_CURRENT_TIME);
            }
            break;
    }
}

/******************************************************************************** app_timerEvent ***
 * Name:        Timer Event Callback
 * Parameters:  lightEvent (true = On, false = Off)
 * Returns:     None
 * Description: 
 **************************************************************************************************/
void app_timerEvent(bool lightEvent) {
    if (lightEvent) {
        lightCtrl = LIGHT_EVENT_ON;
    } else {
        lightCtrl = LIGHT_EVENT_OFF;
    }
}

/****************************************************************************************** main ***
 * Name:        Main Routine
 * Parameters:  None
 * Returns:     None
 * Description: Performs the following:
 *              Initialises PLL to generate an Fosc = 32MHz, Fcy = 16MHz
 **************************************************************************************************/
int main(void) {
    uint8_t i, j;

    /* Clear reset indications */
    RCON = 0;
    
    /* Configure PLL for 32Mhz */
    PLLFBD = FOSC - 2u;
    CLKDIVbits.PLLPOST = 1;
    CLKDIVbits.PLLPRE = 0;
    __builtin_write_OSCCONH(0x03u);
    __builtin_write_OSCCONL(OSCCON | 0x01u);

    /* Wait for Clock switch to occur */
    while (OSCCONbits.COSC != 0b011) {
    }

    /* Wait for PLL to lock */
    while (OSCCONbits.LOCK != 1) {
    }

    /* Set all IO as digital except AN1(RA1)*/
    ANSELA = 0x02;
    ANSELB = 0;
    AD1CON1bits.SSRC = 7; /* Auto Convert */
    AD1CON1bits.ASAM = 1; /* Auto Start */
    AD1CON3bits.SAMC = 5;
    AD1CON3bits.ADCS = 31;
    AD1CHS0 =0x0101;
    AD1CON1bits.ADON = 1;

    /* Set up remappable inputs */
    RPINR18 = 0x0026u; // U1RX

    /* Set up remappable outputs */
    RPOR1bits.RP37R = 1; // U1TX

    /* Initialise Unused Pins as outputs */
    PORTA = 0u;
    TRISA = 0x0000u;
    PORTB &= 0x7FECu;
    TRISB = 0x7FECu;
    
    /* Enable peripherals */
    PMD1 = 0xE65A;
    PMD2 = 0x0F0F;
    PMD3 = 0x0482;
    PMD4 = 0x000C;
    PMD6 = 0x0700;
    PMD7 = 0x0018;

    modbus_reg[MODBUS_BRIGHTNESS] = 50u;
    i = 0u;
    j = MODBUS_DEV_NAME;
    while (defaultName[i] != 0u) {
        modbus_reg[j] = defaultName[i + 1] * 256 + defaultName[i];
        i += 2;
        j++;
    }
    modbus_reg[j] = 0u;

    modbus_reg[MODBUS_DEV_TYPE] = 1u;
    
    /* Initialise Clock and Alarm Times */
    timers_update(8, CURRENT_TIME);
    for (i = START_ALARM_TIMES; i < TIMERS_NUM_TIMERS; i += 2) {
        timers_update(-1, i);
    }

    dimmer_init();
    i2c_vInit();
    modbus_init(NUM_REGS);
    rtcInit();
    XPico_init();
    
    /* Run application */
    while (true) {

        /* Update light settings from timers */
        if (lightCtrl == LIGHT_EVENT_ON) {
            if (!dimmer_getLightState()) {
                if (modbus_reg[MODBUS_LIGHTSWITCH] == 1u) {
                    modbus_reg[MODBUS_LIGHTSWITCH] = 0u;
                    dimmer_setWifiSwitch(false);
                } else {
                    modbus_reg[MODBUS_LIGHTSWITCH] = 1u;
                    dimmer_setWifiSwitch(true);
                }
            }
            lightCtrl = LIGHT_EVENT_IDLE;

        } else if (lightCtrl == LIGHT_EVENT_OFF) {
            if (dimmer_getLightState()) {
                if (modbus_reg[MODBUS_LIGHTSWITCH]) {
                    modbus_reg[MODBUS_LIGHTSWITCH] = 0u;
                    dimmer_setWifiSwitch(false);
                } else {
                    modbus_reg[MODBUS_LIGHTSWITCH] = 1u;
                    dimmer_setWifiSwitch(true);
                }
            }
            lightCtrl = LIGHT_EVENT_IDLE;
        }
        
        if (dimmer_getLightState()) {
            modbus_reg[MODBUS_LIGHTSTATE] = 1u;
        } else {
            modbus_reg[MODBUS_LIGHTSTATE] = 0u;
        }
            
        /* Update timer settings */
        XPico_main();
        
        /* HDC1000 Interface */
        vHdc1000Main();
        
        if (hundredMsecEv) {
            XPico_timer();
            u16Light = ADC1BUF0;
            hundredMsecEv = false;
        }

        if (minuteEv) {
            timers_tick();
            minuteEv = false;
        }
    }

    return 0;
}

/********************************************************************************** vHdc1000Main ***
 * Name:        HDC1000 Interface
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
static void vHdc1000Main(void) {
    switch (u8Hdc1000State) {
        case 0:
            i2c_vTxStart(HDC1000_WR);
            u8Hdc1000State = 1;
            break;
        case 1:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxByte(0x02);
                u8Hdc1000State = 2;
            }
            break;
        case 2:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxByte(0x10);
                u8Hdc1000State = 3;
            }
            break;
        case 3:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxByte(0x00);
                u8Hdc1000State = 4;
            }
            break;
        case 4:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxStop();
                u8Hdc1000State = 5;
            }
        case 5:
            if (i2c_u8GetStatus() == I2C_STATUS_IDLE) {
                bHdc1000Ev = false;
                u8Hdc1000State = 6;
            }
            break;
        case 6:
            if (bHdc1000Ev) {
                i2c_vTxStart(HDC1000_WR);
                bHdc1000Ev = false;
                u8Hdc1000State = 7;
            }
            break;
        case 7:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxByte(0x00);
                u8Hdc1000State = 8;
            }
            break;
        case 8:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vTxStop();
                u8Hdc1000State = 9;
            }
        case 9:
            if (i2c_u8GetStatus() == I2C_STATUS_IDLE) {
                u8Hdc1000State = 10;
            }
            break;
        case 10:
            if (bHdc1000Ev) {
                i2c_vTxStart(HDC1000_RD);
                u8Hdc1000State = 11;
            }
            break;
        case 11:
            if (i2c_u8GetStatus() == I2C_STATUS_ACK) {
                i2c_vReadAck();
                u8Hdc1000State = 12;
            }
            break;
        case 12:
            if (i2c_u8GetStatus() == I2C_STATUS_READ_DONE) {
                u16Temperature = (uint16_t)i2c_u8GetData() << 8;
                i2c_vReadAck();
                u8Hdc1000State = 13;
            }
            break;
        case 13:
            if (i2c_u8GetStatus() == I2C_STATUS_READ_DONE) {
                u16Temperature |= i2c_u8GetData();
                u16Temperature = u16Temperature >> 2;
                i2c_vReadAck();
                u8Hdc1000State = 14;
            }
            break;
        case 14:
            if (i2c_u8GetStatus() == I2C_STATUS_READ_DONE) {
                u16Humidity = (uint16_t)i2c_u8GetData() << 8;
                i2c_vReadNak();
                u8Hdc1000State = 15;
            }
            break;
        case 15:
            if (i2c_u8GetStatus() == I2C_STATUS_READ_DONE) {
                u16Humidity |= i2c_u8GetData();
                u16Humidity = u16Humidity >> 2;
                i2c_vTxStop();
                u8Hdc1000State = 16;
            }
            break;
        case 16:
            if (i2c_u8GetStatus() == I2C_STATUS_IDLE) {
                u8Hdc1000State = 6;
            }
            break;
    }
}
/*************************************************************************************** rtcInit ***
 * Name:        Initialise Timer1 to generate an interrupt every 10msec
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Timer1
 **************************************************************************************************/
static void rtcInit(void) {
    PR1 = RTC_PERIOD;
    T1CONbits.TCKPS = RTC_SCALE;
    T1CONbits.TON = 1;
    IFS0bits.T1IF = 0;
    IPC0bits.T1IP = 5;
    IEC0bits.T1IE = 1;
}

/*
 * End of Workfile: main.c
 */



/***************************************************************************************************
 * Name: Serial Interface
 * Workfile: serial.c
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 02/08/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Release.
 * 17/10/15 | RDTek | Tidied up
 * 07/12/15 | RDTek | Enlarged Buffer Sizes. Updated Interrupt Priority to 6.
 *          |       | Added functions to enable appliction to turn on/off logging functionality.
 *          |       | Corrected error with end of buffer detections. (Memory Leak!!)
 *
 * Description:
 * Interrupt Priority 6
 * 
 **************************************************************************************************/

/**************************************************************************************** Include */
#include <xc.h>
#include "serial.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define CTS_PIN     PORTBbits.RB10
#define RTS_PIN     PORTBbits.RB11

/* NOTE: The settings assume a 16MHz peripheral clock Fp */
#define BAUD_9600   103u
#define BAUD_19200  51u
#define BAUD_125000 7u

#define RX_BUF_MIN_MARGIN   2u
#define RX_BUF_MAX_MARGIN   10u
#define RX_BUF_SIZE         512u
#define TX_BUF_SIZE         512u

#define TSTBUF_SIZE         1024u
/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Variables (static) */
static bool frameErr;
static bool overrun;
static uint16_t rxCnt;
static uint16_t txCnt;

static uint16_t rdIdx;
static uint16_t rxIdx;
static uint16_t txIdx;
static uint8_t rxBuf[RX_BUF_SIZE];
static uint8_t txBuf[TX_BUF_SIZE];

#ifdef TSTBUF
static bool logtxfer;
static uint16_t tstIdx;
static uint8_t tstBuf[TSTBUF_SIZE];
#endif
/********************************************************************** Local Function Prototypes */

/*********************************************************************** Local Constants (static) */

/*************************************************************************** Function Definitions */

/******************************************************************************** _U1RXInterrupt ***
 * Name:        Receive ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: Puts received bytes into a circular buffer ready for collection later.
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _U1RXInterrupt(void) {

    IFS0bits.U1RXIF = 0;

    /* Data received */
    if (U1STAbits.OERR) {
        U1STAbits.OERR = 0;
        (void) U1RXREG;
        overrun = true;
    } else if (U1STAbits.FERR) {
        (void) U1RXREG;
        frameErr = true;
    } else {
        rxBuf[rxIdx] = U1RXREG;
#ifdef TSTBUF
        if (logtxfer) {
            tstBuf[tstIdx] = rxBuf[rxIdx];
            tstIdx++;
            if (tstIdx >= TSTBUF_SIZE) {
                tstIdx = 0;
            }
        }
#endif
        rxCnt++;
        rxIdx++;
        if (rxIdx >= RX_BUF_SIZE) {
            rxIdx = 0u;
        }
        if (rxCnt > (RX_BUF_SIZE - RX_BUF_MIN_MARGIN)) {
            CTS_PIN = 1;
        }
    }
}

/******************************************************************************** _U1TXInterrupt ***
 * Name:        Transmit ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: Handles transmission of individual bytes in transmit message
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _U1TXInterrupt(void) {
    IFS0bits.U1TXIF = 0;
    (void) U1STA;

    if (txCnt == 1) {
        IEC0bits.U1TXIE = 0;
    }
    txCnt--;
    U1TXREG = txBuf[txIdx];
#ifdef TSTBUF
    if (logtxfer) {
        tstBuf[tstIdx] = txBuf[txIdx];
        tstIdx++;
        if (tstIdx >= TSTBUF_SIZE) {
            tstIdx = 0;
        }
    }
#endif
    txIdx++;
}

/******************************************************************************* serial_getRxByte ***
 * Name:        Get Received Byte
 * Parameters:  None
 * Returns:     rxByte
 * Description: Retrieves a data byte from the receive buffer
 **************************************************************************************************/
uint8_t serial_getRxByte(void) {
    uint8_t rval;

    IEC0bits.U1RXIE = 0;
    rval = rxBuf[rdIdx];
    rxCnt--;
    IEC0bits.U1RXIE = 1;
    rdIdx++;
    if (rdIdx >= RX_BUF_SIZE) {
        rdIdx = 0u;
    }
    if (rxCnt < (RX_BUF_SIZE - RX_BUF_MAX_MARGIN)) {
        CTS_PIN = 0;
    }
    return rval;
}

/******************************************************************************* serial_getRxCnt ***
 * Name:        Get Received Count
 * Parameters:  None
 * Returns:     rxCnt
 * Description: Retrieves number of bytes in data buffer
 **************************************************************************************************/
uint16_t serial_getRxCnt(void) {
    return rxCnt;
}

/*********************************************************************************** serial_init ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises UART1 and various GPIO to interface to XPico Device
 **************************************************************************************************/
void serial_init(void) {
    CTS_PIN = 0;
    TRISB &= 0xFBFFu;
    U1MODE = 0x0000u;
    U1BRG = BAUD_9600;
    U1STA = 0x0000U;
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1;
    IPC2bits.U1RXIP = 6;
    IPC3bits.U1TXIP = 6;
    U1MODEbits.UARTEN = 1;
    U1STAbits.UTXEN = 1;

    rdIdx = 0u;
    rxIdx = 0u;
}

#ifdef TSTBUF

/********************************************************************************** serial_logOff ***
 * Name:        Switch Logging Off
 * Parameters:  None
 * Returns:     None
 * Description: Switches off the logger
 **************************************************************************************************/
void serial_logOff(void) {
    logtxfer = false;
}

/*********************************************************************************** serial_logOn ***
 * Name:        Switch Logging On
 * Parameters:  None
 * Returns:     None
 * Description: Switches n the logger
 **************************************************************************************************/
void serial_logOn(void) {
    logtxfer = true;
}
#endif

/********************************************************************************** serial_txMsg ***
 * Name:        Send Message
 * Parameters:  msg - message to transmit
 * Returns:     None
 * Description: Transmits the contents of txBuf upto and including a <LF> character
 **************************************************************************************************/
void serial_txMsg(const uint8_t msg[]) {
    uint8_t i;

    i = 0u;
    while (msg[i] != 0u) {
        txBuf[i] = msg[i];
        i++;
    }
    txCnt = i;
    txIdx = 1u;
    IFS0bits.U1TXIF = 0;
    (void) U1STA;
    U1TXREG = txBuf[0];
#ifdef TSTBUF
    if (logtxfer) {
        tstBuf[tstIdx] = txBuf[0];
        tstIdx++;
        if (tstIdx >= TSTBUF_SIZE) {
            tstIdx = 0;
        }
    }
#endif
    txCnt--;
    if (txCnt > 0u) {
        IEC0bits.U1TXIE = 1;
    }
}

/*
 * End of Workfile: serial.c
 */


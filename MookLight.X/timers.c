/***************************************************************************************************
 * Name: Timers
 * Workfile: timers.c
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 07/12/15 | RDTek | Moved timer functionality from main.
 *
 * Description:
 * Interrupt Priority 5
 * 
 **************************************************************************************************/

/**************************************************************************************** Include */
#include <xc.h>
#include "timers.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define NO_EV 0
#define OFF_EV 1
#define ON_EV 2

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Variables (static) */
static bool event;
static uint8_t timerEvent;
static int16_t timers[TIMERS_NUM_TIMERS];

/********************************************************************** Local Function Prototypes */

/*********************************************************************** Local Constants (static) */

/*************************************************************************** Function Definitions */

/******************************************************************************* timers_getTimer ***
 * Name:        Get Timer
 * Parameters:  idx
 * Returns:     None
 * Description: Get current Timer value
 **************************************************************************************************/
int16_t timers_getTimer(uint16_t idx) {
    return timers[idx];
}

/*********************************************************************************** timers_tick ***
 * Name:        Timers Tick
 * Parameters:  None
 * Returns:     None
 * Description: Handles Timer update every minute
 **************************************************************************************************/
void timers_tick(void) {
    uint8_t i;
    /* Update Timer */
    timers[CURRENT_TIME + 1]++;
    if (timers[CURRENT_TIME + 1] == 1440u) {
        /* One Day */
        timers[CURRENT_TIME + 1] = 0u;
        timers[CURRENT_TIME]++;
    }

    /* Check on alarm times */
    timerEvent = NO_EV;
    i = START_ALARM_TIMES;
    while (i < TIMERS_NUM_TIMERS) {
        event = false;
        if (timers[i] < 7u) {
            /* Weekly alarm */
            if (timers[i] == (timers[CURRENT_TIME] % 7u)) {
                if (timers[i + 1] == timers[CURRENT_TIME + 1]) {
                    event = true;
                }
            }
        } else if (timers[i] == 7u) {
            /* Daily Alarm*/
            if (timers[i + 1] == timers[CURRENT_TIME + 1]) {
                event = true;
            }
        } else {
            /* Other Alarm */
            if ((timers[i] == timers[CURRENT_TIME]) &&
                    (timers[i + 1] == timers[CURRENT_TIME + 1])) {
                event = true;
            }
        }
        if (event) {
            if ((i % 4) == 2) {
                timerEvent = ON_EV;
            } else {
                /* Off Time */
                timerEvent = OFF_EV;
            }
        }
        i += 2;
    }
    if (timerEvent == ON_EV) {
        app_timerEvent(true);
    } else if (timerEvent == OFF_EV) {
        app_timerEvent(false);
    }
}

/********************************************************************************* timers_update ***
 * Name:        Update Timers
 * Parameters:  appTimers
 * Returns:     None
 * Description: Updates Timers
 **************************************************************************************************/
void timers_update(int16_t appTimers, uint16_t idx) {
    timers[idx] = appTimers;
}

/*
 * End of Workfile: timers.c
 */

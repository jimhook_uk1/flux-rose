/***************************************************************************************************
 * Name:    I2C Driver
 * File:    i2c.c
 * Author:  RDTek
 * Date:    15/11/15
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/11/15 | RDTek | Created.
 *
 * Description:
 * Simple I2C Master
 * I2C module gets a clock of Fp/2
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define STATE_IDLE      0
#define STATE_START     1
#define STATE_STOP      2
#define STATE_READ_NAK  3
#define STATE_READ_ACK  4
#define STATE_WAIT_ACK  5
#define STATE_WRITE     6

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static uint8_t u8RxData;
static uint8_t u8I2CStatus;
static uint8_t u8State;
static uint8_t u8StartByte;

/********************************************************************** Local Function Prototypes */

/****************************************************************************** _MI2C1_Interrupt ***
 * Name:        Master I2C1 ISR Handler
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
void __attribute__((interrupt(no_auto_psv))) _MI2C1Interrupt(void) {
    IFS1bits.MI2C1IF = 0;

    (void)I2C1CON;
    (void)I2C1STAT;
    switch(u8State) {
        case STATE_START:
            I2C1TRN = u8StartByte;
            u8State = STATE_WRITE;
            break;
        case STATE_WRITE:
            if (I2C1STATbits.ACKSTAT) {
                u8I2CStatus = I2C_STATUS_NAK;
                u8State = STATE_IDLE;
            } else {
                u8I2CStatus = I2C_STATUS_ACK;
                u8State = STATE_IDLE;
            }
            break;
        case STATE_READ_ACK:
            u8RxData = I2C1RCV;
            I2C1CONbits.ACKDT = 0;
            I2C1CONbits.ACKEN = 1;
            u8State = STATE_WAIT_ACK;
            break;
        case STATE_READ_NAK:
            u8RxData = I2C1RCV;
            I2C1CONbits.ACKDT = 1;
            I2C1CONbits.ACKEN = 1;
            u8State = STATE_WAIT_ACK;
            break;
        case STATE_WAIT_ACK:
            u8I2CStatus = I2C_STATUS_READ_DONE;
            u8State = STATE_IDLE;
            break;
        case STATE_STOP:
            u8I2CStatus = I2C_STATUS_IDLE;
            u8State = STATE_IDLE;
            break;
        case STATE_IDLE:
        default:
            break;
    }
}

/********************************************************************************* i2c_u8GetData ***
 * Name:        Get Data
 * Parameters:  None
 * Returns:     u8RxData
 * Description: Retrieves data from the buffer
 **************************************************************************************************/
uint8_t i2c_u8GetData(void) {
    return u8RxData;
}

/******************************************************************************** i2c_u8GetStatus***
 * Name:        Get Status
 * Parameters:  None
 * Returns:     u8I2CStatus
 * Description: Reads I2C drive status
 **************************************************************************************************/
uint8_t i2c_u8GetStatus(void) {
    return u8I2CStatus;
}

/************************************************************************************* i2c_vInit ***
 * Name:        Initialise I2c Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the I2c interface
 **************************************************************************************************/
void i2c_vInit(void) {
    I2C1BRG = I2C_BRG;
    I2C1CONbits.I2CEN = 1;
    IFS1bits.MI2C1IF = 0;
    IPC4bits.MI2C1IP = 5;
    IEC1bits.MI2C1IE = 1;
    u8I2CStatus = I2C_STATUS_IDLE;
}

/********************************************************************************** i2c_vReadAck ***
 * Name:        Read Data transmit Ack
 * Parameters:  None
 * Returns:     None
 * Description: Starts an I2C read transfer and responds with Ack
 **************************************************************************************************/
void i2c_vReadAck(void) {
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_READ_ACK;
    I2C1CONbits.RCEN = 1;
}

/********************************************************************************** i2c_vReadNak ***
 * Name:        Read Data transmit Nak
 * Parameters:  None
 * Returns:     None
 * Description: Starts an I2C read transfer and responds with Nak
 **************************************************************************************************/
void i2c_vReadNak(void) {
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_READ_NAK;
    I2C1CONbits.RCEN = 1;
}

/*********************************************************************************** i2c_vTxByte ***
 * Name:        Transmit Byte
 * Parameters:  byte
 * Returns:     None
 * Description: Transmits 'byte' and waits for ack or nak
 *************************************************************************************************/
void i2c_vTxByte(uint8_t byte) {
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_WRITE;
    I2C1TRN = byte;
}

/******************************************************************************** i2c_vTxReStart ***
 * Name:        Transmit ReStart
 * Parameters:  startByte
 * Returns:     None
 * Description: Transmits an I2C ReStart followed by 'startByte' and waits for ack or nak
 *************************************************************************************************/
void i2c_vTxReStart(uint8_t startByte) {
    u8StartByte = startByte;
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_START;
    I2C1CONbits.RSEN = 1;
}

/********************************************************************************** i2c_vTxStart ***
 * Name:        Transmit Start
 * Parameters:  startByte
 * Returns:     None
 * Description: Transmits an I2C Start followed by 'startByte' and waits for ack or nak
 *************************************************************************************************/
void i2c_vTxStart(uint8_t startByte) {
    u8StartByte = startByte;
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_START;
    I2C1CONbits.SEN = 1;
    //while (I2C1STATbits.S == 0);
    //I2C1TRN = u8StartByte;
    //u8State = STATE_WRITE;
}

/********************************************************************************** i2c_vTxStop ***
 * Name:        Transmit Stop
 * Parameters:  None
 * Returns:     None
 * Description: Transmits an I2C Stop
 *************************************************************************************************/
void i2c_vTxStop(void) {
    u8I2CStatus = I2C_STATUS_TFER_IN_PROGRESS;
    u8State = STATE_STOP;
    I2C1CONbits.PEN = 1;
}

/*
 * End of Workfile: i2c.c
 */

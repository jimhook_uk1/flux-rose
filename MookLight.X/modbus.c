/***************************************************************************************************
 * Name: ModBus
 * Workfile: modbus.c
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 30/07/15 | RDTek | Created.
 * 14/09/15 | RDTek | First Relese
 * 17/10/15 | RDTek | Tidied up.
 * 07/12/15 | RDTek | Removed modbus register definitions. Added callbacks to get/set registers
 *          |       | values from App. Added init function to pass number of registers.
 * 
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include <xc.h>
#include <string.h>
#include "modbus.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* Command Codes */
#define READ_HOLDING_REGISTERS      0x03u
#define WRITE_SINGLE_REGISTER       0x06u
#define WRITE_MULTIPLE_REGISTERS    0x10u
#define READ_STRING                 0x64u
#define WRITE_STRING                0x65u
/* */
#define ILLEGAL_FUNCTION            0x01u
#define ILLEGAL_DATA_ADDRESS        0x02u
#define ILLEGAL_DATA_VALUE          0x03u
#define SERVER_DEVICE_FAILURE       0x04u

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Variables (static) */
static uint16_t maxNumReg;

/********************************************************************** Local Function Prototypes */
static void copybuf2reg(uint16_t startAdd, uint8_t src[], uint8_t num);
static void copyreg2buf(uint8_t dest[], uint16_t startAdd, uint8_t num);

/*********************************************************************** Local Constants (static) */

/*************************************************************************** Function Definitions */

/************************************************************************************ cpybuf2reg ***
 * Name:        Copy Buffer to Register
 * Parameters:  startAdd
 *              src[]
 *              num - number of words
 * Returns:     None
 * Description: Copies num words from src to dest changing word from little-Endian to big-Endian in
 *              the process
 **************************************************************************************************/
static void copybuf2reg(uint16_t startAdd, uint8_t src[], uint8_t num) {
    uint16_t a;
    uint8_t i, j;

    j = 0u;
    a = startAdd;
    for (i = 0u; i < num; i++) {
        app_putReg(a, (int16_t)src[j] * 256u + src[j + 1]);
        j += 2;
        a++;
    }
}

/************************************************************************************ cpyreg2buf ***
 * Name:        Copy Register to Buffer
 * Parameters:  dest[]
 *              src[]
 *              num - number of words
 * Returns:     None
 * Description: Copies num words from src to dest changing word from little-Endian to big-Endian in
 *              the process
 **************************************************************************************************/
static void copyreg2buf(uint8_t dest[], uint16_t startAdd, uint8_t num) {
    int16_t v;
    uint16_t a;
    uint8_t i, j;

    j = 0u;
    a = startAdd;
    for (i = 0u; i < num; i++) {
        v = app_getReg(a);
        dest[j] = (uint8_t)(v / 256u);
        j++;
        dest[j] = (uint8_t)(v % 256u);
        j++;
        a++;
    }
}

/*********************************************************************************** modbus_init ***
 * Name:        Initialise
 * Parameters:  numReg  - Number of registers
 * Returns:     None
 * Description: Initilises the modbus processor
 **************************************************************************************************/
void modbus_init(uint16_t numReg) {
    maxNumReg = numReg;
}

/***************************************************************************** modbus_processMsg ***
 * Name:        Process Message
 * Parameters:  txBuf   - Transmit Buffer
 *              rxBuf   - Received message
 * Returns:     None
 * Description: Gets the status of the message transmitter
 **************************************************************************************************/
void modbus_processMsg(uint8_t txBuf[], uint8_t rxBuf[]) {
    uint16_t txLength;
    uint8_t regLen;
    uint16_t startAdd;
    uint8_t err;

    /* Copy transaction identifier */
    txBuf[0] = rxBuf[0];
    txBuf[1] = rxBuf[1];

    err = 0U;
    txBuf[7] = rxBuf[7];
    startAdd = ((uint16_t) rxBuf[8] << 8) +
            rxBuf[9];
    switch (rxBuf[7]) {
        case READ_HOLDING_REGISTERS:
            regLen = rxBuf[11];
            if ((regLen == 0u) ||
                    (regLen > 0x7Du)) {
                err = ILLEGAL_DATA_VALUE;
            } else if ((startAdd >= maxNumReg) ||
                    ((startAdd + regLen) > maxNumReg)) {
                err = ILLEGAL_DATA_ADDRESS;
            } else {
                txBuf[8] = regLen * 2u;
                copyreg2buf(&txBuf[9], startAdd, regLen);
                txLength = txBuf[8] + 2u;
            }
            break;
        case WRITE_SINGLE_REGISTER:
            if (startAdd >= maxNumReg) {
                err = ILLEGAL_DATA_ADDRESS;
            } else {
                copybuf2reg(startAdd, &rxBuf[10], 1u);
                txBuf[8] = rxBuf[8];
                txBuf[9] = rxBuf[9];
                txBuf[10] = rxBuf[10];
                txBuf[11] = rxBuf[11];
                txLength = 5u;
            }
            break;
        case WRITE_MULTIPLE_REGISTERS:
            regLen = rxBuf[11];
            if ((regLen == 0u) ||
                    (regLen > 0x7Bu) ||
                    (regLen * 2u) != rxBuf[12]) {
                err = ILLEGAL_DATA_VALUE;
            } else if ((startAdd >= maxNumReg) ||
                    ((startAdd + regLen) > maxNumReg)) {
                err = ILLEGAL_DATA_ADDRESS;
            } else {
                copybuf2reg(startAdd, &rxBuf[13], regLen);
                txBuf[8] = rxBuf[8];
                txBuf[9] = rxBuf[9];
                txBuf[10] = rxBuf[10];
                txBuf[11] = rxBuf[11];
                txLength = 5u;
            }
            break;
        default:
            err = ILLEGAL_FUNCTION;
            break;
    }
    if (err != 0u) {
        txBuf[7] = rxBuf[7] + 0x80u;
        txBuf[8] = err;
        txLength = 2u;
    }
    txLength++;
    txBuf[4] = (uint8_t)(txLength >> 8);
    txBuf[5] = (uint8_t) txLength;
}

/*
 * End of Workfile: modbus.c
 */

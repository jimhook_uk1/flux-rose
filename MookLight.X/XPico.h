/***************************************************************************************************
 * Name: XPcio Data Link Layer Header
 * Workfile: XPico.h
 * Author: RDTek
 * Date: 07/12/15
 *
 * Copyright:  RD Technical Solutions
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 14/09/15 | RDTek | Created.
 * 17/10/15 | RDTek | Tidied up.
 * 07/12/15 | RDTek | Removed app_modbusMsgRxd. Modbus callbacks now generated by modbus module.
 *          |       | Added XPico_timer to enable timeout processing.
 *
 * Description:
 **************************************************************************************************/

#ifndef XPICO_H
#define XPICO_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/********************************************************************* Global Function Prototypes */

/************************************************************************** dimmer_getLightState ***
 ** Name:        Get Light State
 ** Parameters:  None
 ** Returns:     true if light is on
 ** Description: Gets current light State
 **************************************************************************************************/
extern bool dimmer_getLightState(void);

/************************************************************************************ XPico_init ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises specified SCI to operate as a generic test port
 **************************************************************************************************/
extern void XPico_init(void);

/************************************************************************************ XPico_main ***
 ** Name:        Main task
 ** Parameters:  None
 ** Returns:     None
 ** Description: Called every 10msecs
 **************************************************************************************************/
extern void XPico_main(void);

/*********************************************************************************** XPico_timer ***
 ** Name:        Timer task
 ** Parameters:  None
 ** Returns:     None
 ** Description: Called every 100msecs
 **************************************************************************************************/
extern void XPico_timer(void);

#endif /* XPICO_H */
/*
 * End of Workfile: XPico.h
 */

